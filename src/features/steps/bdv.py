# -*- coding: utf-8 -*-
from behave import given, when, then, use_step_matcher

from data.data_bdv import Data_bdv
from functions.ComponentsHelper import ComponentsHelper as oComponentsHelper
from functions.WaitElements import WaitElements as oWaitElement

use_step_matcher("re")


class testequipo():

    @step("I set whole questions as NO")
    def step_impl(self):
        for intTotalElement in range(1, 70):
            oWaitElement.fn_specific_time(self, 0.4)
            # oWaitElement.fn_wait_element(self, Data_bdv.QUESTIONS)
            oComponentsHelper.fn_click_element(self, Data_bdv.QUESTIONS, str([intTotalElement]))
            if intTotalElement == 10 or intTotalElement == 20 or intTotalElement == 30 or intTotalElement == 40 or intTotalElement == 50 or intTotalElement == 60 or intTotalElement == 69:
                oWaitElement.fn_specific_time(self, 0.5)
                oWaitElement.fn_wait_element(self, Data_bdv.NEXT_STEP)
                oComponentsHelper.fn_click_element(self, Data_bdv.NEXT_STEP)


    @step("I accept initials from (.*) to (.*)")
    def step_impl(self, initial, final):
        for intTotalElement in range(int(initial),int(final)):
            oWaitElement.fn_specific_time(self, 0.6)
            oComponentsHelper.fn_click_element(self, Data_bdv.INITIAL, str([intTotalElement]))


    @step("I fill BDV in (.*) field with data")
    def step_impl(context, locator):
        print(context.db)
        oWaitElement.fn_wait_element(context, locator)
        oComponentsHelper.fn_send_key_text(context, locator, context.db)


    @step("I complete the form work experience")
    def step_impl(self):
        oWaitElement.fn_wait_element(self, 'Input Job Title')
        oComponentsHelper.fn_send_key_text(self, 'Input Job Title', 'Tester')
        oComponentsHelper.fn_send_key_text(self, 'Input Name', 'Umvel')
        oComponentsHelper.fn_send_key_text(self, 'Input Business Type', 'IT')
        oComponentsHelper.fn_send_key_text(self, 'Input Address 1', 'Ámsterdam 173 Hipódromo ')
        oComponentsHelper.fn_send_key_text(self, 'Input Address 2', ' Interior 3')
        oComponentsHelper.fn_send_key_text(self, 'Input City', 'Ciudad de México, CDMX')
        oComponentsHelper.fn_send_key_text(self, 'Input State', 'Alcaldía Cuauhtémoc')
        oComponentsHelper.fn_send_key_text(self, 'Input ZIP', '06100')


    @step("I select dates")
    def step_impl(self):
        oComponentsHelper.fn_select_by_text(self, 'Start Year', '2020')
        oComponentsHelper.fn_select_by_text(self, 'Start Month', 'September')
        oComponentsHelper.fn_select_by_text(self, 'Start Day', '19')