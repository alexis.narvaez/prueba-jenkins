from behave import *

from functions.Validations import Validations as oValidations
from functions.WindowHandle import WindowHandle as oWindowHandle
from functions.ComponentsHelper import ComponentsHelper as oComponentsHelper
from functions.WaitElements import WaitElements as oWaitElement
from functions.Jscript import Jscript as oJscript
import random

use_step_matcher("re")

class testviva():

    @step("I valid the title of the view: (.*) - (.*)")
    def step_impl(self, locator, expected):
        oValidations.fn_assert_text(self, locator, expected)


    @step("I switch to frame: (.*)")
    def step_impl(self, locator):
        oWindowHandle.fn_switch_to_iframe(self, locator)


    @step("I switch new page")
    def step_impl(self):
        oWindowHandle.fn_switch_new_window(self)


    @step("I scroll and click: (.*)")
    def step_impl(self, locator):
        oComponentsHelper.fn_select_element_dropdown(self, locator)


    @step("I fill VIVA in (.*) field with data")
    def step_impl(context, locator):
        print(context.db)
        oWaitElement.fn_wait_element(context, locator)
        oComponentsHelper.fn_send_key_text(context, locator, context.db)


    @step("I check if sections exist")
    def step_impl(self):
        oValidations.fn_assert_text(self, 'Personal information', 'Información personal')
        oValidations.fn_assert_text(self, 'Contact Information', 'Información de contacto')
        oJscript.fn_scroll_to_bottom(self)
        oValidations.fn_assert_text(self, 'Travel Documents', 'Documentos de viaje')