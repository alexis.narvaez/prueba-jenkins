# -*- coding: utf-8 -*-
from behave import given, when, then, use_step_matcher
from functions.Selenium import Selenium as oSelenium
from functions.ComponentsHelper import ComponentsHelper as oComponentsHelper
from functions.Jscript import Jscript as oJscript
from functions.Validations import Validations as oValidations
from functions.WaitElements import WaitElements as oWaitElement
from functions.Reporter import Reporter as oReporter
from functions.GetJsons import GetJsons as oGetJson
from functions.Initialize import Initialize
from functions.WindowHandle import WindowHandle as oWindowHandle
import os

use_step_matcher("re")


class testequipo():

    @given("I open the URL: (.*)")
    def step_impl(self, url):
        oSelenium.fn_open_browser(self, url)

    @when("I load the Json file: (.*)")
    def step_impl(self, json):
        oGetJson.fn_get_json_file(self, json)

    @step("I fill in (.*) field with data: (.*)")
    def step_impl(self, locator, text):
        oWaitElement.fn_wait_element(self, locator)
        oComponentsHelper.fn_send_key_text(self, locator, text)

    @step("I click on (.*)")
    def step_impl(self, locator):
        oWaitElement.fn_wait_element(self, locator)
        oComponentsHelper.fn_click_element(self, locator)

    @then("Page is loaded with title: (.*)")
    def step_impl(self, title):
        oValidations.fn_assert_exact_title(self, title)

    @step("I wait (.*) seconds")
    def step_impl(self, time):
        oWaitElement.fn_specific_time(self, int(time))

    @step("I take a screenshot")
    def step_impl(self):
        oReporter.fn_screenshot_allure(self, "Evidence")

    @step("I click JS on (.*)")
    def step_impl(self, locator):
        oJscript.fn_js_click(self, locator)

    @step("I select from What is your current occuppation: (.*)")
    def step_impl(self, locator):
        oComponentsHelper.fn_click_element(self, locator)

    @step("I select from Do you require any special accommodations: (.*)")
    def step_impl(self, locator):
        oComponentsHelper.fn_click_element(self, locator)

    @step(
        "I select from Do you have any physical limitations which prevent you from being able to work in an unskilled, full time job: (.*)")
    def step_impl(self, locator):
        oComponentsHelper.fn_click_element(self, locator)

    @step("I select from Do you have a current US visa: (.*)")
    def step_impl(self, locator):
        oComponentsHelper.fn_click_element(self, locator)

    @step("I fill_1 in (.*) field with data: (.*)")
    def step_impl(self, locator, text):
        root_dir = Initialize.basedir
        path = os.path.join(root_dir, "files", text)
        oComponentsHelper.fn_send_key_text(self, locator, path)

    @step("I scroll bottom")
    def step_impl(self):
        oJscript.fn_scroll_to_bottom(self)

    @step("I switch to iframe: (.*)")
    def step_impl(self, locator):
        oWindowHandle.fn_switch_to_iframe(self, locator)


    @step("I scrolling vertical inside: (.*)")
    def step_impl(self, locator):
        oComponentsHelper.fn_scroll_inside(self, locator)


    @step("I switch frame credit card: (.*)")
    def step_impl(self, locator):
        oWindowHandle.fn_switch_to_iframe(self, locator)


    @step("I back to main content")
    def step_impl(self):
        oWindowHandle.fn_switch_to_default_content(self)


    @step("I select (.*) on (.*)")
    def step_impl(self, locator, text):
        oComponentsHelper.fn_select_by_text(self, locator, text)