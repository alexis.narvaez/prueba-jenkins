# -*- coding: utf-8 -*-
from behave import given, when, then, use_step_matcher
from functions.Selenium import Selenium as oSelenium
from functions.ComponentsHelper import ComponentsHelper as oComponentsHelper
from functions.Jscript import Jscript as oJscript
from functions.Validations import Validations as oValidations
from functions.Reporter import Reporter as oReporter
from data.Data import Data as oData
from functions.WaitElements import WaitElements as oWaitElement
from functions.Randoms import Randoms as oRandoms
from functions.Reporter import Reporter as oReporter
from functions.WindowHandle import WindowHandle as oWindowHandle
from functions.GetJsons import GetJsons as oGetJson
from functions.GetProperties import GetProperty as oGetProperty
import os
use_step_matcher("re")

class testequipo():

    @given("abro la URL: (.*)")
    def step_impl(self, url):
        oSelenium.fn_open_browser(self, url)

    @step("Doy clic en (.*)")
    def step_impl(self, locator):
        oComponentsHelper.fn_click_element(self, locator)

    @then("Se carga la página con titulo: (.*)")
    def step_impl(self, titulo):
        oValidations.fn_assert_exact_title(self, titulo)

    @step("Cargo el Json de la página: (.*)")
    def step_impl(self, json):
        oGetJson.fn_get_json_file(self, json)

    @step("Escribo el (.*) en el campo (.*)")
    def step_impl(self, text, locator):
        oComponentsHelper.fn_send_key_text(self, locator, text)

    @step("Escribo en la pagina logInMyStore0 el (.*)")
    def step_impl(self, locator):
        oComponentsHelper.fn_send_key_text(self, locator, "rosasdurazno")

    @then("Se muestra el (.*) de alerta (.*)")
    def step_impl(self, locator, str_expected_text):
        oValidations.fn_validate_match(self, locator, str_expected_text)

    @step("Escribo en el campo (.*) la sentencia (.*)")
    def step_impl(self, locator, text):
        oComponentsHelper.fn_send_key_text(self, locator, text)

    @step("Escribo en el campo (.*) la contraseña de mi usuario")
    def step_impl(self, locator):
        oComponentsHelper.fn_send_key_text(self, locator, oData.PASSWORD_FABO)

    @step("Creo un enunciado como el de Luis (.*) (.*)")
    def step_impl(self, locator, text):
        print("algo")

    @step("Tomo una captura de pantalla")
    def step_impl(self):
        oReporter.fn_screenshot_allure(self, "Evidence")
        
    @step("Lleno el campo (.*) con el dato: (.*)")
    def step_impl(self, locator, text):
        oComponentsHelper.fn_send_key_text(self, locator, text)

    @step("Escribo en el campo (.*) la contrasenia")
    def step_impl(self, locator):
        oComponentsHelper.fn_send_key_text(self, locator, "Admin1234.")
