# Created by Fabián Solano at 16/05/2022
Feature: Escenario Mariela

    @ttf
    Scenario: Validar ingreso a la app con usuario vacío
        Given abro la URL: http://automationpractice.com/index.php
        When Cargo el Json de la página: Ejemplo/Header
        And Doy clic en Sign in
        Then Se carga la página con titulo: Login - My Store
        When Cargo el Json de la página: Ejemplo/LogInMyStore
        And Escribo en el campo Password la contraseña de mi usuario
        And Doy clic en Sign In
        Then Se muestra el Mensaje de alerta An email address required.