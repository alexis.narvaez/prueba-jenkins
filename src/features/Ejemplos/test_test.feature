
@regresion
Feature: Como cliente de la pagian automation practice quiero iniciar sesión para validar diferentes usuarios y contraseñas x

    @hp
    Scenario: Validar ingreso a la app con credenciales válidas
        Given abro la URL: http://automationpractice.com/index.php
        When Cargo el Json de la página: Ejemplo/Header
        And Doy clic en Sign in
        Then Se carga la página con titulo: Login - My Store
        When Cargo el Json de la página: Ejemplo/LogInMyStore
        And Escribo en el campo Email Address la sentencia cacheater@gmail.com
        And Escribo en el campo Password la contraseña de mi usuario
        And Doy clic en Sign In
        Then Se carga la página con titulo: My account - My Store

    @hp
    Scenario: ML
        Given abro la URL: https://www.mercadolibre.com.mx/
        And Tomo una captura de pantalla
        When Cargo el Json de la página: ML/Header
        And Escribo en el campo Buscar la sentencia text


    @ttf
    Scenario: Validar ingreso a la app con usuario vacío
        Given abro la URL: http://automationpractice.com/index.php
        When Cargo el Json de la página: Ejemplo/Header
        And Doy clic en Sign in
        Then Se carga la página con titulo: Login - My Store
        When Cargo el Json de la página: Ejemplo/LogInMyStore
        And Escribo en el campo Password la contraseña de mi usuario
        And Doy clic en Sign In
        Then Se muestra el Mensaje de alerta An email address required.

    @ttf
    Scenario: Validar ingreso a la app con contraseña vacía
        Given abro la URL: http://automationpractice.com/index.php
        When Cargo el Json de la página: Ejemplo/Header
        And Doy clic en Sign in
        Then Se carga la página con titulo: Login - My Store
        When Cargo el Json de la página: Ejemplo/LogInMyStore
        And Escribo en el campo Email Address la sentencia cacheater@gmail.com
        And Doy clic en Sign In
        Then Se muestra el Mensaje de alerta Password is required.

    @ttf
    Scenario: Validar ingreso a la app con usuario mal ingresado
        Given abro la URL: http://automationpractice.com/index.php
        When Cargo el Json de la página: Ejemplo/Header
        And Doy clic en Sign in
        Then Se carga la página con titulo: Login - My Store
        And Cargo el Json de la página: Ejemplo/LogInMyStore
        And Escribo en el campo Email Address la sentencia cacheatergmail.com
        And Escribo en el campo Password la sentencia P@55w0rd
        And Doy clic en Sign In
        Then Se muestra el Mensaje de alerta Invalid email address.

    @ttf
    Scenario: Validar ingreso a la app con usuario no existe
        Given abro la URL: http://automationpractice.com/index.php
        When Cargo el Json de la página: Ejemplo/Header
        And Doy clic en Sign in
        Then Se carga la página con titulo: Login - My Store
        When Cargo el Json de la página: Ejemplo/LogInMyStore
        And Escribo el asdfasdf@gmail.com en el campo Email Address
        And Escribo en el campo Password la sentencia P@55w0rd
        And Doy clic en Sign In
        Then Se muestra el Mensaje de alerta Authentication failed.

    @ttf
    Scenario: Validar ingreso a la app con contraseña incorrecta
        Given abro la URL: http://automationpractice.com/index.php
        When Cargo el Json de la página: Ejemplo/Header
        And Doy clic en Sign in
        Then Se carga la página con titulo: Login - My Store
        And Cargo el Json de la página: Ejemplo/LogInMyStore
        And Escribo en el campo Email Address la sentencia cacheater@gmail.com
        And Escribo en el campo Password la sentencia P@55w0rd
        And Doy clic en Sign In
        Then Se muestra el Mensaje de alerta Authentication failed.

    @ttf
    Scenario Outline: Validar ingreso a la app con credenciales varios usuario y passwords incorrectos
        Given abro la URL: http://automationpractice.com/index.php
        When Cargo el Json de la página: Ejemplo/Header
        And Doy clic en Sign in
        Then Se carga la página con titulo: Login - My Store
        When Cargo el Json de la página: Ejemplo/LogInMyStore
        And Escribo en el campo Email Address la sentencia <Email Address>
        And Escribo en el campo Password la sentencia <Password>
        And Doy clic en Sign In
        Then Se muestra el Mensaje de alerta Authentication failed.
        Examples:
            | Email Address |       Password  |
            |cacheater@gmail.com|   abcde32 |
            |fabian@gmail.com|      fsasd24 |
            |redes@gmail.com|       adoiii234 |
            |afsd@gmail.com|        audio123 |

    @ttp
    Scenario Outline: Validar ingreso a la app con credenciales varios usuario y passwords correctos
        Given abro la URL: http://automationpractice.com/index.php
        When Cargo el Json de la página: Ejemplo/Header
        And Doy clic en Sign in
        Then Se carga la página con titulo: Login - My Store
        When Cargo el Json de la página: Ejemplo/LogInMyStore
        And Escribo en el campo Email Address la sentencia <Email Address>
        And Escribo en el campo Password la sentencia <Password>
        And Doy clic en Sign In
        Then Se carga la página con titulo: My account - My Store
        Examples:
            | Email Address |                   Password  |
            |cacheater@gmail.com|               rosasdurazno |
            |fabian.solano@umvel.com|               umvelfsa |
