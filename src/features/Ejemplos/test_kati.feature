# Created by Katia Garcia at 17/05/2022
Feature: Bimbo Login Admin
  
Scenario: Login correcto
            Given abro la URL: http://ger2021-admin-staging.s3-website-us-east-1.amazonaws.com/#/
            When Cargo el Json de la página: Bimbo/Bimbo_login
            And Lleno el campo Correo con el dato: miguelalopezg96@gmail.com
            And Lleno el campo Password con el dato: Password1.
            And Doy clic en Login
            Then Se carga la página con titulo: Administrador - Bimbo GER
            

Scenario: Login sin colocar Correo
            Given abro la URL: http://ger2021-admin-staging.s3-website-us-east-1.amazonaws.com/#/
            When Cargo el Json de la página: Bimbo/Bimbo_login
            And Lleno el campo Password con el dato: Password1.
            And Doy clic en Login
            Then Se muestra el Mensaje de alerta El Correo es un campo requerido