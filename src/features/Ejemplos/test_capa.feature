# Created by Fabián Solano at 26/05/2022
Feature: Scenarios Capa
  # Enter feature description here

  @hp
  Scenario: HP Capa 1
        Given abro la URL: https://staging.frontend.capa.umvel.dev/admin/
        When Cargo el Json de la página: Ejemplo/capa_admin_login
        And Escribo en el campo Email Address la sentencia evelyn.francisco+30@umvel.com
        #And Escribo en el campo Password la sentencia Admin1234.
        And Escribo en el campo Password la contrasenia
        And Doy clic en Signin
        Then Se carga la página con titulo: CAPA - Administrator
        And I wait 12 seconds
        And I take a screenshot

