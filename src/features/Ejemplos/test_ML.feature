
@regresion
Feature: Prueba con ML

    @hp
    Scenario: Prueba de ML
        Given abro la URL: https://www.mercadolibre.com.mx/
        When Cargo el Json de la página: ML/Header
        And Escribo en el campo Buscar la sentencia smarwatch
        And Doy clic en Lupa
        Then Se carga la página con titulo: Smarwatch | MercadoLibre 📦
        When Cargo el Json de la página: ML/busquedas
        And Doy clic en 1er elemento de la lista
        And I take a screenshot
        And I switch to iframe: frame
        And Escribo en el campo Car Number la sentencia 2111351684685132
