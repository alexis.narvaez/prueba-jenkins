import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from selenium.common.exceptions import InvalidSessionIdException, NoSuchWindowException
#from src.functions.Selenium import Selenium as oSelenium
#from src.functions.Reporter import Reporter as oReporter
from functions.Selenium import Selenium as oSelenium
from functions.Reporter import Reporter as oReporter
from behave.contrib.scenario_autoretry import patch_scenario_with_autoretry
import random

def before_all(context):
    n = str(random.randint(10, 500))
    text = 'testaccountcp' + n + '@yopmail.com'
    context.db = text

def after_scenario(self, scenario):
    if scenario.status == 'failed':
        #oReporter.fn_screenshot_allure(self, f'Algo fue mal en el escenario: {scenario.__name__}')
        oReporter.fn_screenshot_allure(self, f'Algo fue mal con este escenario...')
        oSelenium.fn_tear_down(self)
        #scenario.mark_skipped()
    elif scenario.status == 'passed':
        oSelenium.fn_tear_down(self)

def before_feature(self, feature):
    for scenario in feature.scenarios:
        #if "hp" in scenario.effective_tags or "ttp" in scenario.effective_tags or "ttf" in scenario.effective_tags:
        #if "hp" in scenario.effective_tags or "ttf" in scenario.effective_tags:
        if "ttf" in scenario.effective_tags:
            patch_scenario_with_autoretry(scenario, max_attempts=2)

def after_step(self, scenario):
    if os.environ.get('status_execution_failed') == "false":
        assert "a" == "b", "Ocurrio un error en este escenario"
        #oSelenium.fn_tear_down(self)
        #scenario.mark_skipped()
        #self.browser = oSelenium.fn_get_driver(self)
        #self.skip(require_not_executed=True)
