# Created by Fabián Solano at 22/04/2022
@regresion
Feature: Client AOS- Plan A Credit or Debit Card Scenarios
  # Enter feature description here

  @ttf
  Scenario: 1 - Create Account - Client AOS- Plan A Credit or Debit Card
    Given I open the URL: https://staging.client.bdvs.jaque.dev/login
    When I load the Json file: bdv/login
    And I click on Create new account
    When I load the Json file: bdv/Create_an_account
    And I wait 5 seconds
    And I fill in Choose your country of birth field with data: Mexico
    And I click on Mexico
    And I take a screenshot
    And I click on Next
    Then Page is loaded with title: Create account - Yow by BDV
    And I fill in First name field with data: Aldo
    And I fill in Last name field with data: Collazos
    And I fill BDV in Email field with data
    And I fill in Password field with data: Password1.
    And I take a screenshot
    And I click on Create account
    Then Page is loaded with title: Yow by BDV

  @ttf
  Scenario: 2 | 3 - Questionnaire - Information Record | Client AOS - Plan A Credit or Debit Card
    Given I open the URL: https://staging.client.bdvs.jaque.dev/login
    When I load the Json file: bdv/login
    And I fill BDV in Email field with data
    And I click on Login1
    And I fill in Password field with data: Password1.
    And I click on Login2
    Then Page is loaded with title: Yow by BDV
    When I load the Json file: bdv/welcomeback
    And I click on Complete the Questionnaire
    And I take a screenshot
    When I load the Json file: bdv/on_boarding_process
    And I click JS on Continue
    When I load the Json file: bdv/terms_and_conditions
    And I click on I have reviewed and understand all materials shown above
    And I click on I accept the terms and contitions
    And I click on Continue
    Then Page is loaded with title: Yow by BDV
    When I load the Json file: bdv/questionnaire
    And I set whole questions as NO
    Then Page is loaded with title: Yow by BDV
    When I load the Json file: bdv/user_information
    And I wait 15 seconds
    And I fill in Current country of residence field with data: Mexico
    And I click on Mexico
    And I fill in Street address field with data: Norte
    And I fill in City field with data: Mexico
    And I fill in Province field with data: cdmx
    And I fill in ZipCode field with data: 5565546
    And I click on Primary phone number
    And I click on American Samoa
    And I fill in Phone number field with data: 15151651511516616
    And I select from What is your current occuppation: Job
    And I scroll bottom
    And I fill in Field of work field with data: test
    And I fill in Job position field with data: test
    And I select from Do you require any special accommodations: No1
    And I select from Do you have any physical limitations which prevent you from being able to work in an unskilled, full time job: No2
    And I select from Do you have a current US visa: No3
    And I fill_1 in Attachment field with data: demo.jpg
    And I click JS on I understand consent information
    And I wait 5 seconds
    And I click JS on Final Continue
    And I take a screenshot
    And I wait 3 seconds
    And I click on Send
    And I take a screenshot


  @ttf
  Scenario: 4 - Customer Type Selection - Client AOS - Plan A Credit or Debit Card
    Given I open the URL: https://staging.admin.bdvs.jaque.dev/login
    When I load the Json file: bdv/login
    And I fill in Email field with data: webmaster@grupojaque.com
    And I fill in Password field with data: Password1.
    And I click on Login3
    #Then Page is loaded with title: (28) Yow Admin Portal
    And I wait 3 seconds
    When I load the Json file: bdv/client_dashboard
    And I click on Leads
    And I wait 5 seconds
    And I click on First Element
    And I wait 3 seconds
    And I click on Basic Documents
    And I scrolling vertical inside: Progress
    And I click on Consular Processing
    And I click JS on Approve
    And I take a screenshot
    And I click on Confirm Approve
    And I wait 5 seconds
    And I take a screenshot

  @ttf
  Scenario: 5 | 6 | 7 - Job Offer - Payment Plan - Contract Signing - Client AOS - Plan A Credit or Debit Card
    Given I open the URL: https://staging.client.bdvs.jaque.dev/login
    When I load the Json file: bdv/login
    And I fill BDV in Email field with data
    And I click on Login1
    And I fill in Password field with data: Password1.
    And I click on Login2
    Then Page is loaded with title: Yow by BDV
    When I load the Json file: bdv/job
    And I take a screenshot
    And I click on Go to Job
    And I click on IT Job
    And I click on Apply & Reserve
    And I click on Agree & apply
    And I click on Select Contract
    And I wait 20 seconds
    And I click on Disclosure Accepted
    And I click on Continue
    And I click on Initial1
    And I wait 3 seconds
    And I click on Accept Initials
    And I accept initials from 2 to 15
    And I click on Firm
    And I take a screenshot
    And I click on Finish
    And I wait 10 seconds

  @ttf
  Scenario: 8 - Payment Method - Client AOS - Plan A Credit or Debit Card
    Given I open the URL: https://staging.client.bdvs.jaque.dev/login
    When I load the Json file: bdv/login
    And I fill BDV in Email field with data
    And I click on Login1
    And I fill in Password field with data: Password1.
    And I click on Login2
    Then Page is loaded with title: Yow by BDV
    When I load the Json file: bdv/reserve_spot
    And I click on Continue
    And I click on Submit Payment
    And I click on Pay Credit Card
    And I take a screenshot
    And I fill in Name field with data: Aldo Collazos
    And I switch frame credit card: Frame Credit Card
    And I fill in Card Element field with data: 4242424242424242
    And I fill in MMYY field with data: 0928
    And I fill in CVC field with data: 777
    And I fill in ZIP field with data: 90000
    And I take a screenshot
    And I back to main content
    And I click on Pay now
    And I wait 10 seconds
    And I click on Go to summary
    And I valid the title of the view: Summary Label - Thank you! we have received your payment. We need 2-3 days to confirm it, we will notify you.

  @ttf
  Scenario: 9 - Accept payment - Client AOS - Plan A Credit or Debit Card
    Given I open the URL: https://staging.admin.bdvs.jaque.dev/login
    When I load the Json file: bdv/login
    And I fill in Email field with data: webmaster@grupojaque.com
    And I fill in Password field with data: Password1.
    And I click on Login3
    #Then Page is loaded with title: (28) Yow Admin Portal
    And I wait 3 seconds
    When I load the Json file: bdv/client_dashboard
    And I click on Leads
    And I wait 5 seconds
    And I click on First Element
    And I wait 3 seconds
    And I click on Payments
    And I click on Go Payments
    And I take a screenshot
    And I click JS on Approve
    And I click on Approve Payment
    And I wait 15 seconds
    And I take a screenshot

  @ttf
  Scenario: 10 - Complete my information  - Client AOS - Plan A Credit or Debit Card
    Given I open the URL: https://staging.client.bdvs.jaque.dev/login
    When I load the Json file: bdv/login
    And I fill BDV in Email field with data
    And I click on Login1
    And I fill in Password field with data: Password1.
    And I click on Login2
    #Then Page is loaded with title: Yow by BDV
    When I load the Json file: bdv/complete_information
    And I click on Complete my information
    And I wait 3 seconds
    And I scroll bottom
    And I click on Select Country
    And I wait 10 seconds
    And I click on Albania
    And I select Select Years on 2000
    And I select Select Months on April
    And I select Select Days on 10
    And I fill in Class Admission field with data: F-2
    And I take a screenshot
    And I click JS on Next Step
    And I wait 3 seconds
    And I click on Add work experience
    And I complete the form work experience
    And I fill in Input Hours field with data: 40
    And I click on Select Country Experience
    And I click on Albania
    And I select dates
    And I fill in Job Details field with data: Realizar pruebas a los sistemas
    And I take a screenshot
    And I click JS on Save
    And I wait 7 seconds
    And I click on Next Step 2
    And I click on Confirm & Share
    And I wait 5 seconds
    And I valid the title of the view: Label Complete - Thank you for verifying your personal information and work experience.

  @ttf
  Scenario: 11 - ETA 9089 - Request - Client AOS - Plan A Credit or Debit Card
    Given I open the URL: https://staging.admin.bdvs.jaque.dev/login
    When I load the Json file: bdv/login
    And I fill in Email field with data: webmaster@grupojaque.com
    And I fill in Password field with data: Password1.
    And I click on Login3
    #Then Page is loaded with title: (28) Yow Admin Portal
    When I load the Json file: bdv/client_dashboard
    And I wait 5 seconds
    And I scroll bottom
    And I click on First Element
    And I wait 3 seconds
    And I click on Preview ETA
    And I click on Request
    And I wait 2 seconds
    And I click on Preview ETA
    And I click on Yes
    And I scrolling vertical inside: Container Progress
    And I click on Open Calendar
    And I wait 2 seconds
    And I click JS on Today
    And I fill in A-Number field with data: 435231
    And I fill_1 in ETA-PDF field with data: dol11.pdf
    And I take a screenshot
    And I click on Confirm Filing
    And I wait 10 seconds
    And I click on LC Approved
    And I scrolling vertical inside: Container Progress
    And I click on LC Approved Date
    And I wait 2 seconds
    And I click on Day 30
    And I fill_1 in Certified 9089 field with data: dol11.pdf
    And I take a screenshot
    And I click on Confirm Approval
    And I wait 5 seconds