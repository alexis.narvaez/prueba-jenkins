# Created by Alexis at 21/06/2022
@regresion
Feature: Consultar el perfil de usuario VIVAWEB
  # Enter feature description here
  @ttf
  Scenario: CP01_Perfil / Perfil / Consultar perfil.
    Given I open the URL: https://viva-v1-staging.jaque.dev/es-mx/
    When I load the Json file: viva/login
    And I click on Go to new account
    And I fill in Login Name field with data: pruebavivapp99@yopmail.com
    And I fill in Login Password field with data: Pozol123-
    And I click on Login Inicio
    And I wait 10 seconds
    And I click on Icon Profile
    And I click on Go Profile
    When I load the Json file: viva/profile
    And I valid the title of the view: Label Profile Detail - Detalles del perfil

  @ttf
  Scenario: CP02_Perfil / Perfil / Consultar perfil / Consultar Detalles de Perfil
    Given I open the URL: https://viva-v1-staging.jaque.dev/es-mx/
    When I load the Json file: viva/login
    And I click on Accept Cookies
    And I click on Go to new account
    And I fill in Login Name field with data: pruebavivapp99@yopmail.com
    And I fill in Login Password field with data: Pozol123-
    And I click on Login Inicio
    And I wait 10 seconds
    And I click on Icon Profile
    And I click on Go Profile
    When I load the Json file: viva/profile
    And I click on Detail Profile
    And I check if sections exist