# Created by Alexis at 08/06/2022
@regresion
Feature: Inicio de sesión en VIVAWEB
  # Enter feature description here
  @ttf
  Scenario: CP01_Login | Login / Cuenta nueva.
    Given I open the URL: https://viva-v1-staging.jaque.dev/es-mx/
    When I load the Json file: viva/login
    And I click on Accept Cookies
    And I click on Go to new account
    And I click on Create new account
    And I valid the title of the view: New account Label - Cuenta nueva

  @ttf
  Scenario: CP02_Login | Login / Crear cuenta a través de Corrreo electrónico.
    Given I open the URL: https://viva-v1-staging.jaque.dev/es-mx/
    When I load the Json file: viva/login
    And I click on Accept Cookies
    And I click on Go to new account
    And I click on Create new account
    And I fill VIVA in Email field with data
    And I fill in Password field with data: Prueba123-
    And I fill in Confirm Password field with data: Prueba123-
    And I take a screenshot
    And I click on Continue
    And I valid the title of the view: Confirm Email - Confirma tu correo electrónico

  @ttf
  Scenario: CP03_Login | Login / Crear cuenta a través de Correo electrónico / Confirmar identidad.
    Given I open the URL: https://yopmail.com/es/
    When I load the Json file: viva/yopmail
    And I fill VIVA in Email field with data
    And I click on Arrow
    And I take a screenshot
    And I switch to frame: Yopmail
    And I click on Confirm
    And I switch new page
    And I wait 20 seconds
    And I take a screenshot

  @ttf
  Scenario: CP0_Login | Login / Registro información personal del usuario
    Given I open the URL: https://viva-v1-staging.jaque.dev/es-mx/
    When I load the Json file: viva/login
    And I click on Go to new account
    And I fill VIVA in Login Name field with data
    And I fill in Login Password field with data: Prueba123-
    And I click on Login Inicio
    And I wait 10 seconds
    When I load the Json file: viva/new-account
    And I fill in First Name field with data: Alexis
    And I fill in Last Name field with data: Narvaez
    And I click on Date Birth
    And I click on Years
    And I click on Year
    And I click on Month
    And I click on Day
    And I click on Nationality
    And I scroll and click: Zimbabue
    And I click on Create Account
    And I wait 6 seconds
    And I click on Icon Profile
    And I take a screenshot

  @ttf
  Scenario: CP06_Login | Login / Iniciar sesión con Correo electrónico.
    Given I open the URL: https://viva-v1-staging.jaque.dev/es-mx/
    When I load the Json file: viva/login
    And I click on Go to new account
    And I fill VIVA in Login Name field with data
    And I fill in Login Password field with data: Prueba123-
    And I click on Login Inicio
    And I wait 10 seconds
    And I click on Icon Profile
    And I take a screenshot

  @ttf
  Scenario: CP09_Login | Login / Cerrar sesión.
    Given I open the URL: https://viva-v1-staging.jaque.dev/es-mx/
    When I load the Json file: viva/login
    And I click on Go to new account
    And I fill VIVA in Login Name field with data
    And I fill in Login Password field with data: Prueba123-
    And I click on Login Inicio
    And I wait 10 seconds
    And I click on Icon Profile
    And I take a screenshot
    And I click on Log out
    And I click on Go to new account
    And I valid the title of the view: Login Label - Inicia sesión
    And I take a screenshot

  @ttf
  Scenario: CP10_Login | Login / Olvide mi contraseña.
    Given I open the URL: https://viva-v1-staging.jaque.dev/es-mx/
    When I load the Json file: viva/login
    And I click on Accept Cookies
    And I click on Go to new account
    And I click on Recover
    And I fill VIVA in Email field with data
    And I click on Send
    And I valid the title of the view: Inbox Label - Revisa tu bandeja de entrada

  @ttf
  Scenario: CP11_Login | Login / Olvide mi contraseña / Cambiar contraseña.
    Given I open the URL: https://yopmail.com/es/
    When I load the Json file: viva/yopmail
    And I fill VIVA in Email field with data
    And I click on Arrow
    And I switch to frame: Yopmail
    And I click on Change Password
    And I switch new page
    And I wait 5 seconds
    And I fill in Password field with data: Pruebita12345-
    And I fill in Confirm Password field with data: Pruebita12345-
    And I click on Save
    And I wait 3 seconds
    And I valid the title of the view: Success - Exito

  @ttf
  Scenario: CP13_Login | Login / Crear cuenta a través de Corrreo electrónico / Captura de correo ya registrado.
    Given I open the URL: https://viva-v1-staging.jaque.dev/es-mx/
    When I load the Json file: viva/login
    And I click on Accept Cookies
    And I click on Go to new account
    And I click on Create new account
    And I fill VIVA in Email field with data
    And I fill in Password field with data: Prueba123-
    And I fill in Confirm Password field with data: Prueba123-
    And I take a screenshot
    And I click on Continue
    And I valid the title of the view: Existing account - Ya existe una cuenta con este correo electrónico.

  @ttf
  Scenario: CP14_Login | Login / Datos no permitidos.
    Given I open the URL: https://viva-v1-staging.jaque.dev/es-mx/
    When I load the Json file: viva/login
    And I click on Go to new account
    And I fill in Login Name field with data: asd
    And I valid the title of the view: Email Validation - Dirección de correo electrónico inválida
    And I fill in Login Password field with data: asd
    And I click on Login Inicio
    And I valid the title of the view: Password Validation - Tu contraseña es demasiado corta. Debe tener al menos 8 caracteres

  @ttf
  Scenario: CP15_Login | Login / Datos obligatorios.
    Given I open the URL: https://viva-v1-staging.jaque.dev/es-mx/
    When I load the Json file: viva/login
    And I click on Go to new account
    And I click on Login Inicio
    And I valid the title of the view: Email Validation - Este campo es obligatorio.
    And I valid the title of the view: Password Validation - Este campo es obligatorio.

  @ttf
  Scenario: CP16_Login | Login / Correo o contraseña invalidos.
    Given I open the URL: https://viva-v1-staging.jaque.dev/es-mx/
    When I load the Json file: viva/login
    And I click on Accept Cookies
    And I click on Go to new account
    And I fill VIVA in Login Name field with data
    And I fill in Login Password field with data: Pozole123213-
    And I click on Login Inicio
    And I valid the title of the view: Data verification - Correo o contraseña inválidos. Verifica que tus datos sean correctos.

  @ttf
  Scenario: CP18_Login | Login / Iniciar sesión con Cuenta empresa
    Given I open the URL: https://viva-v1-staging.jaque.dev/es-mx/?loginType=Enterprise
    When I load the Json file: viva/login
    And I click on Go to new account
    And I fill in Login Name field with data: VBWEB
    And I fill in Login Password field with data: Viva001+
    And I click on Login Inicio
    And I wait 10 seconds
    And I click on Icon Profile
    And I take a screenshot

  @ttf
  Scenario: CP19_Login | Login / Iniciar sesión con Cuenta Agencia
    Given I open the URL: https://viva-v1-staging.jaque.dev/es-mx/?loginType=Agency
    When I load the Json file: viva/login
    And I click on Go to new account
    And I fill in Login Name field with data: VBWEBAG
    And I fill in Login Password field with data: Web1234+
    And I click on Login Inicio
    And I wait 10 seconds
    And I click on Icon Agency
    And I take a screenshot

  @ttf
  Scenario: CP20_Login | Login / Iniciar sesión con Cuenta staff
    Given I open the URL: https://viva-v1-staging.jaque.dev/es-mx/?loginType=Staff
    When I load the Json file: viva/login
    And I click on Go to new account
    And I fill in Login Name field with data: 55161914
    And I fill in Login Password field with data: Rainbow6*
    And I click on Login Inicio
    And I wait 10 seconds
    And I click on Icon Agency
    And I take a screenshot