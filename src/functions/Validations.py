import time
import os

import allure
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from functions.Selenium import Selenium as oSelenium
from selenium.webdriver.support import expected_conditions as EC
from functions.GetJsons import GetJsons as oGetJson
from functions.Reporter import Reporter as oReporter
from functions.GetProperties import GetProperty as oGetProperty

import pytest
import unittest

class Validations(oSelenium):

    def fn_validate_element_is_present(self, locator):  # devuelve true o false
        self.json_value_to_find, self.json_get_field_by, self.json_get_obj_name = oGetJson.fn_get_entity(self, locator)

        if self.json_get_field_by is None:
            print("No se encontro el valor en el Json definido")
        else:
            try:
                if self.json_get_field_by.lower() == "id":
                    wait = WebDriverWait(self.driver, 30)
                    wait.until(EC.visibility_of_element_located((By.ID, self.json_value_to_find)))
                    print(u"check_element: Se visualizo el elemento con nombre " + self.json_get_obj_name +" locator " + locator)
                    return True

                elif self.json_get_field_by.lower() == "name":
                    wait = WebDriverWait(self.driver, 30)
                    wait.until(EC.visibility_of_element_located((By.NAME, self.json_value_to_find)))
                    #print(u"check_element: Se visualizo el elemento " + locator)
                    print(u"check_element: Se visualizo el elemento con nombre " + self.json_get_obj_name + " locator " + locator)
                    return True

                elif self.json_get_field_by.lower() == "xpath":
                    wait = WebDriverWait(self.driver, 30)
                    wait.until(EC.visibility_of_element_located((By.XPATH, self.json_value_to_find)))
                    print(u"check_element: Se visualizo el elemento con nombre " + self.json_get_obj_name +" locator " + locator)
                    return True

                elif self.json_get_field_by.lower() == "link":
                    wait = WebDriverWait(self.driver, 30)
                    wait.until(EC.visibility_of_element_located((By.LINK_TEXT, self.json_value_to_find)))
                    print(u"check_element: Se visualizo el elemento con nombre " + self.json_get_obj_name +" locator " + locator)
                    return True

                elif self.json_get_field_by.lower() == "css":
                    wait = WebDriverWait(self.driver, 30)
                    wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, self.json_value_to_find)))
                    print(u"check_element: Se visualizo el elemento con nombre " + self.json_get_obj_name +" locator " + locator)
                    return True

            except NoSuchElementException:
                oReporter.fn_screenshot_allure(self, "Elemento no presente: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " NoSuchElementException")
                print("Elemento no presente: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " NoSuchElementException")
                os.environ['status_execution_failed'] = "false"
                return False
            except TimeoutException:
                oReporter.fn_screenshot_allure(self, "Elemento no presente: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " TimeoutException")
                print("Elemento no presente: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " TimeoutException")
                os.environ['status_execution_failed'] = "false"
                return False

    def fn_assert_text(self, locator, EXPECTED):

        self.json_value_to_find, self.json_get_field_by, self.json_get_obj_name = oGetJson.fn_get_entity(self, locator)

        if self.json_get_field_by is None:
            print("No se encontro el valor en el Json definido")
        else:
            if self.json_get_field_by.lower() == "id":
                wait = WebDriverWait(self.driver, 20)
                wait.until(EC.presence_of_element_located((By.ID, self.json_value_to_find)))
                obj_text = self.driver.find_element_by_id(self.json_value_to_find).text

            elif self.json_get_field_by.lower() == "name":
                wait = WebDriverWait(self.driver, 20)
                wait.until(EC.presence_of_element_located((By.NAME, self.json_value_to_find)))
                obj_text = self.driver.find_element_by_name(self.json_value_to_find).text

            elif self.json_get_field_by.lower() == "xpath":
                wait = WebDriverWait(self.driver, 20)
                wait.until(EC.presence_of_element_located((By.XPATH, self.json_value_to_find)))
                obj_text = self.driver.find_element_by_xpath(self.json_value_to_find).text

            elif self.json_get_field_by.lower() == "link":
                wait = WebDriverWait(self.driver, 20)
                wait.until(EC.presence_of_element_located((By.PARTIAL_LINK_TEXT, self.json_value_to_find)))
                obj_text = self.driver.find_element_by_partial_link_text(self.json_value_to_find).text

            elif self.json_get_field_by.lower() == "css":
                wait = WebDriverWait(self.driver, 20)
                wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, self.json_value_to_find)))
                obj_text = self.driver.find_element_by_css_selector(self.json_value_to_find).text

        oReporter.fn_screenshot_allure(self, "Verificar Texto: el valor mostrado en: " + locator + " es: " + obj_text + " el esperado es: " + EXPECTED)
        print("Verificar Texto: el valor mostrado en: " + locator + " es: " + obj_text + " el esperado es: " + EXPECTED)
        assert EXPECTED == obj_text, "Los valores comparados no coinciden"


    def fn_assert_exact_title(self, expected):
        time.sleep(2)
        actual_title = self.driver.title
        print("actual " + actual_title)
        if str(actual_title) == str(expected):
            print("Assert Correcto")
            validate_match_message(actual_title, expected, "Title_Page")
            oReporter.fn_screenshot_allure(self, "Evidence")
            assert actual_title in expected, "El valor esperado concuerda: " + expected
        else:
            print(f'El valor esperado NO concuerda: Esperado--> {expected}  Actual--> {actual_title}')
            validate_match_message(actual_title, expected, "Title_Page")
            oReporter.fn_screenshot_allure(self, "Evidence")
            assert actual_title in expected, f'El valor esperado NO concuerda: Esperado--> {expected}  Actual--> {actual_title}'
            #os.environ['status_execution_failed'] = "false"

    def fn_assert_partial_title_b_f(self, expected):
        import re

        actual_title = self.driver.title
        pattern_search = fr"(?<=|)\w+"

        matching = re.findall(pattern_search, actual_title, re.IGNORECASE)

        intCoincidencia = 0
        for result in matching:
            #print(result)
            intCoincidencia = intCoincidencia + 1

        #print(intCoincidencia)
        if intCoincidencia > 0:
            print("Assert Correcto")
            assert actual_title in actual_title, "El valor esperado concuerda: " + expected
            oReporter.fn_screenshot_allure(self, "Evidence")
        else:
            oReporter.fn_screenshot_allure(self, "Evidence")
            print(f'El valor esperado NO concuerda: Esperado--> {expected}  " Actual--> " {actual_title}')
            assert actual_title in expected, f'El valor esperado NO concuerda: Esperado--> {expected}  " Actual--> " + {actual_title}'
            #os.environ['status_execution_failed'] = "false"


    def fn_assert_in_string(self, str_actual, str_expected):
        time.sleep(1)
        str_actual = str_actual.strip().lower()
        str_expected = str_expected.strip().lower()
        int_match = str_expected.find(str_actual)

        if int_match > 0:
            print("Assert Correcto")
            assert str_actual in str_actual, "El valor esperado concuerda: " + str_expected
        else:
            print(f'El valor esperado NO concuerda: Esperado--> {str_expected}  Actual--> {str_actual}')
            oReporter.fn_screenshot_allure(self, f'El valor esperado NO concuerda: Esperado--> {str_expected}  Actual--> {str_actual}')
            assert str_actual in str_expected, f'El valor esperado NO concuerda: Esperado--> {str_expected}  Actual--> {str_actual}'
            #os.environ['status_execution_failed'] = "false"

    def fn_assert_string(self, str_actual, str_expected):
        time.sleep(1)
        str_actual = str_actual.strip().lower()
        str_expected = str_expected.strip().lower()

        if str_actual == str_expected:
            print("Assert Correcto")
            assert str_actual in str_actual, "El valor esperado concuerda: " + str_expected
        else:
            print(f'El valor esperado NO concuerda: Esperado--> {str_expected}  Actual--> {str_actual}')
            oReporter.fn_screenshot_allure(self, f'El valor esperado NO concuerda: Esperado--> {str_expected}  Actual--> {str_actual}')
            assert str_actual in str_expected, f'El valor esperado NO concuerda: Esperado--> {str_expected}  Actual--> {str_actual}'
            #os.environ['status_execution_failed'] = "false"


    def fn_validate_match(self, entity, str_expected):
        #str_actual = oGetProperty.fn_get_value(self, entity)
        str_obj_name = oGetJson.fn_get_obj_name(self, entity)
        str_actual = oGetProperty.fn_get_text(self, entity)
        if str_actual == str_expected:
            print("Assert Correcto")
            validate_match_message(str_actual, str_expected, str_obj_name)
            oReporter.fn_screenshot_allure(self, "Evidence")
            assert str_actual in str_expected, "El valor esperado concuerda: " + str_expected
        else:
            validate_match_message(str_actual, str_expected, str_obj_name)
            oReporter.fn_screenshot_allure(self, f'El dato no concuerda con lo capturado, campo: {entity}, esperado: {str_expected}')
            assert str_actual in str_expected, f'El valor esperado NO concuerda: Esperado--> {str_expected}  Actual--> {str_actual}'
            #os.environ['status_execution_failed'] = "false"


    def fn_validate_not_match(self, entity, str_expected):
        str_actual = oGetProperty.fn_get_text(self, entity)
        if str_actual != str_expected:
            print("Assert Correcto")
            pass
        else:
            oReporter.fn_screenshot_allure(self, f'El dato no concuerda con lo capturado, campo: {entity}, esperado: {str_expected}')
            #os.environ['status_execution_failed'] = "false"

@allure.step
def validate_match_message(current_message, expected_message, element):
    pass