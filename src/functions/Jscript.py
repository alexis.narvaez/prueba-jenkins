from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from functions.ComponentsHelper import ComponentsHelper as oComponentsHelper
from functions.Selenium import Selenium as oSelenium
from selenium.webdriver.support import expected_conditions as EC
from functions.WaitElements import WaitElements as oWaitElement
from functions.GetJsons import GetJsons as oGetJason
from functions.Reporter import Reporter as oReporter
import time
import os


class Jscript(oSelenium):

    def fn_new_window(self, URL):
        self.driver.execute_script(f'''window.open("{URL}","_blank");''')
        #self.fn_page_has_loaded(self)

    def fn_page_has_loaded(self):
        time.sleep(1)
        driver = self.driver
        print("Checking if {} page is loaded.".format(self.driver.current_url))
        page_state = driver.execute_script('return document.readyState;')
        yield
        WebDriverWait(driver, 12).until(lambda driver: page_state == 'complete')
        assert page_state == 'complete', "No se completo la carga"
        return self.driver

    def fn_scroll_to(self, locator):
        #get_entity = oComponentsHelper.fn_get_entity(self, locator)
        self.json_get_field_by, self.json_value_to_find, self.json_get_obj_name = oGetJason.fn_get_entity(self, locator)

        if self.json_get_field_by is None:
            return print("No se encontro el valor en el Json definido")
        else:
            try:
                if self.json_get_field_by.lower() == "id": #json_GetFieldBy.lower() == "id"
                    localizador = self.driver.find_element(By.ID, self.json_value_to_find)
                    self.driver.execute_script("arguments[0].scrollIntoView();", localizador)
                    print(u"scroll_to: " + locator)
                    return True

                elif self.json_get_field_by.lower() == "xpath":
                    localizador = self.driver.find_element(By.XPATH, self.json_value_to_find)
                    self.driver.execute_script("arguments[0].scrollIntoView();", localizador)
                    print(u"scroll_to: " + locator)
                    return True

                elif self.json_get_field_by.lower() == "link":
                    localizador = self.driver.find_element(By.PARTIAL_LINK_TEXT, self.json_value_to_find)
                    self.driver.execute_script("arguments[0].scrollIntoView();", localizador)
                    print(u"scroll_to: " + locator)
                    return True

            except TimeoutException:
                oReporter.fn_screenshot_allure(self, "scroll_to: No presente " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                print(u"scroll_to: No presente " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                os.environ['status_execution_failed'] = "false"

    def fn_scroll_to_bottom(self):
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(1)

    def fn_scroll_to_top(self):
        self.driver.execute_script("scroll(0, 0);")

    def fn_js_click(self, locator, add_locator=None, MyTextElement=None):
        self.json_get_field_by, self.json_value_to_find, self.json_get_obj_name = oGetJason.fn_get_entity(self, locator)

        oWaitElement.fn_wait_element(self, locator, MyTextElement)

        if self.json_get_field_by is None:
            return print("No se encontro el valor en el Json definido")
        else:
            try:
                if self.json_get_field_by.lower() == "id":
                    localizador = self.driver.find_element(By.ID, self.json_value_to_find)
                    self.driver.execute_script("arguments[0].click();", localizador)
                    print(u"Se hizo click en: " + locator)
                    return True

                if self.json_get_field_by.lower() == "xpath":
                    if MyTextElement is not None:
                        self.json_value_to_find = self.json_value_to_find.format(MyTextElement)
                        print(self.json_value_to_find)

                    elif add_locator is not None:
                        element = (self.json_value_to_find)
                        print(element + add_locator)
                        localizador = self.driver.find_element_by_xpath(element + add_locator)
                        self.driver.execute_script("arguments[0].click();", localizador)
                        print(u"Se hizo click en: " + locator)
                        return True

                    else:
                        localizador = self.driver.find_element(By.XPATH, self.json_value_to_find)
                        self.driver.execute_script("arguments[0].click();", localizador)
                        print(u"Se hizo click en: " + locator)
                        return True

                if self.json_get_field_by.lower() == "link":
                    localizador = self.driver.find_element(By.PARTIAL_LINK_TEXT, self.json_value_to_find)
                    self.driver.execute_script("arguments[0].click();", localizador)
                    print(u"Se hizo click en: " + locator)
                    return True

                if self.json_get_field_by.lower() == "name":
                    localizador = self.driver.find_element(By.NAME, self.json_value_to_find)
                    self.driver.execute_script("arguments[0].click();", localizador)
                    print(u"Se hizo click en: " + locator)
                    return True

                if self.json_get_field_by.lower() == "css":
                    localizador = self.driver.find_element(By.CSS_SELECTOR, self.json_value_to_find)
                    self.driver.execute_script("arguments[0].click();", localizador)
                    print(u"Se hizo click en: " + locator)
                    return True

            except TimeoutException:
                oReporter.fn_screenshot_allure(self, u"js_clic: No presente " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                print(u"js_clic: No presente " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                os.environ['status_execution_failed'] = "false"


    def fn_js_sendkeys(self, locator, str_value, MyTextElement=None):
        self.json_get_field_by, self.json_value_to_find, self.json_get_obj_name = oGetJason.fn_get_entity(self, locator)

        oWaitElement.fn_wait_element(self, locator, MyTextElement)

        if self.json_get_field_by is None:
            return print("No se encontro el valor en el Json definido")
        else:
            try:
                time.sleep(1)
                if self.json_get_field_by.lower() == "id":
                    localizador = self.driver.find_element(By.ID, self.json_value_to_find)
                    self.driver.execute_script("arguments[0].click();", localizador)
                    self.driver.execute_script("arguments[0].setAttribute('value', '" + str_value +"')", localizador)
                    print(u"Se envia data en: " + locator)
                    return True

                if self.json_get_field_by.lower() == "xpath":
                    if MyTextElement is not None:
                        self.json_value_to_find = self.json_value_to_find.format(MyTextElement)
                        print(self.json_value_to_find)

                    localizador = self.driver.find_element(By.XPATH, self.json_value_to_find)
                    self.driver.execute_script("arguments[0].click();", localizador)
                    self.driver.execute_script("arguments[0].setAttribute('value', '" + str_value +"')", localizador)
                    print(u"Se envia data en: " + locator)
                    return True

                if self.json_get_field_by.lower() == "link":
                    localizador = self.driver.find_element(By.PARTIAL_LINK_TEXT, self.json_value_to_find)
                    self.driver.execute_script("arguments[0].click();", localizador)
                    self.driver.execute_script("arguments[0].setAttribute('value', '" + str_value +"')", localizador)
                    return True

                if self.json_get_field_by.lower() == "name":
                    localizador = self.driver.find_element(By.NAME, self.json_value_to_find)
                    self.driver.execute_script("arguments[0].click();", localizador)
                    self.driver.execute_script("arguments[0].setAttribute('value', '" + str_value +"')", localizador)
                    print(u"Se envia data en: " + locator)
                    return True

                if self.json_get_field_by.lower() == "css":
                    localizador = self.driver.find_element(By.CSS_SELECTOR, self.json_value_to_find)
                    self.driver.execute_script("arguments[0].click();", localizador)
                    self.driver.execute_script("arguments[0].setAttribute('value', '" + str_value +"')", localizador)
                    print(u"Se envia data en: " + locator)
                    return True

            except TimeoutException:
                oReporter.fn_screenshot_allure(self, u"js_clic: No presente " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                print(u"js_clic: No presente " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                os.environ['status_execution_failed'] = "false"


    def fn_js_select_option(self, locator, option_visible_text, MyTextElement=None):
        self.json_get_field_by, self.json_value_to_find, self.json_get_obj_name = oGetJason.fn_get_entity(self, locator)

        oWaitElement.fn_wait_element(self, locator, MyTextElement)

        if self.json_get_field_by is None:
            return print("No se encontro el valor en el Json definido")
        else:
            try:
                time.sleep(1)
                if self.json_get_field_by.lower() == "id":
                    localizador = self.driver.find_element(By.ID, self.json_value_to_find)
                    #self.driver.execute_script("arguments[0].click();", localizador)
                    #self.driver.execute_script("arguments[0].setAttribute('value', '" + str_value +"')", localizador)
                    self.driver.execute_script("var select = arguments[0]; for(var i = 0; i < select.options.length; i++){ if(select.options[i].text == arguments[1]){ select.options[i].selected = true; } }", localizador, option_visible_text)
                    print(u"Se seleecciona el valor en el localizador: " + locator)
                    return True

                if self.json_get_field_by.lower() == "xpath":
                    if MyTextElement is not None:
                        self.json_value_to_find = self.json_value_to_find.format(MyTextElement)
                        print(self.json_value_to_find)

                    localizador = self.driver.find_element(By.XPATH, self.json_value_to_find)
                    self.driver.execute_script("var select = arguments[0]; for(var i = 0; i < select.options.length; i++){ if(select.options[i].text == arguments[1]){ select.options[i].selected = true; } }", localizador, option_visible_text)
                    print(u"Se seleecciona el valor en el localizador: " + locator)
                    return True

                if self.json_get_field_by.lower() == "link":
                    localizador = self.driver.find_element(By.PARTIAL_LINK_TEXT, self.json_value_to_find)
                    self.driver.execute_script("var select = arguments[0]; for(var i = 0; i < select.options.length; i++){ if(select.options[i].text == arguments[1]){ select.options[i].selected = true; } }", localizador, option_visible_text)
                    print(u"Se seleecciona el valor en el localizador: " + locator)
                    return True

                if self.json_get_field_by.lower() == "name":
                    localizador = self.driver.find_element(By.NAME, self.json_value_to_find)
                    self.driver.execute_script("var select = arguments[0]; for(var i = 0; i < select.options.length; i++){ if(select.options[i].text == arguments[1]){ select.options[i].selected = true; } }", localizador, option_visible_text)
                    print(u"Se seleecciona el valor en el localizador: " + locator)
                    return True

                if self.json_get_field_by.lower() == "css":
                    localizador = self.driver.find_element(By.CSS_SELECTOR, self.json_value_to_find)
                    self.driver.execute_script("var select = arguments[0]; for(var i = 0; i < select.options.length; i++){ if(select.options[i].text == arguments[1]){ select.options[i].selected = true; } }", localizador, option_visible_text)
                    print(u"Se seleecciona el valor en el localizador: " + locator)
                    return True

            except TimeoutException:
                oReporter.fn_screenshot_allure(self, u"js_clic: No presente " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                print(u"js_clic: No presente " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                os.environ['status_execution_failed'] = "false"

