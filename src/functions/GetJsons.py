from functions.Initialize import Initialize
from functions.Selenium import Selenium as oSelenium

import json
import pytest

class GetJsons(oSelenium):
    # *************************************************************************
    #    Json Handle

    def fn_get_json_file(self, file):
        json_path = Initialize.Json + "/" + file + '.json'
        print("Json path " + json_path)
        try:
            with open(json_path, "r") as read_file:
                self.json_strings = json.loads(read_file.read())
                print("get_json_file: " + json_path)
        except FileNotFoundError:
            self.json_strings = False
            pytest.skip(u"get_json_file: No se encontro el Archivo " + file)
            oSelenium.fn_tear_down()

    def fn_get_entity(self, entity):
        if self.json_strings is False:
            print("Define el DOM para esta prueba")
        else:
            try:
                self.json_value_to_find = self.json_strings[entity]["valueToFind"]
                self.json_get_field_by = self.json_strings[entity]["getFieldBy"]
                self.json_get_obj_name = self.json_strings[entity]["objName"]
                print(self.json_strings[entity]["getFieldBy"])
                print(self.json_strings[entity]["valueToFind"])
                print(self.json_strings[entity]["objName"])
                return self.json_value_to_find, self.json_get_field_by, self.json_get_obj_name

            except KeyError:
                pytest.skip(u"get_entity: No se encontro la key a la cual se hace referencia: " + entity)
                oSelenium.fn_tear_down()
                return None

    def fn_get_obj_name(self, entity):
        if self.json_strings is False:
            print("Define el DOM para esta prueba")
        else:
            try:
                self.json_value_to_find = self.json_strings[entity]["valueToFind"]
                self.json_get_field_by = self.json_strings[entity]["getFieldBy"]
                self.json_get_obj_name = self.json_strings[entity]["objName"]
                print(self.json_strings[entity]["getFieldBy"])
                print(self.json_strings[entity]["valueToFind"])
                print(self.json_strings[entity]["objName"])
                return self.json_get_obj_name

            except KeyError:
                pytest.skip(u"get_entity: No se encontro la key a la cual se hace referencia: " + entity)
                oSelenium.fn_tear_down()
                return None