from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import TimeoutException, NoSuchElementException, StaleElementReferenceException
from functions.ComponentsHelper import ComponentsHelper as oComponentsHelper
from functions.Selenium import Selenium as oSelenium
from selenium.webdriver.support import expected_conditions as EC
from functions.GetJsons import GetJsons as oGetJson
from functions.Reporter import Reporter as oReporter
import time
import os

class WaitElements(oSelenium):

    def fn_wait_element(self, locator, MyTextElement=None):
        self.json_value_to_find, self.json_get_field_by, self.json_get_obj_name = oGetJson.fn_get_entity(self, locator)
        #time.sleep(1)
        if self.json_get_field_by is None:
            return print("No se encontro el valor en el Json definido")
        else:
            try:
                if self.json_get_field_by.lower() == "id":
                    wait = WebDriverWait(self.driver, 20)
                    wait.until(EC.visibility_of_element_located((By.ID, self.json_value_to_find)))
                    wait.until(EC.element_to_be_clickable((By.ID, self.json_value_to_find)))
                    print(u"Esperar_Elemento: Se visualizo el elemento " + locator)
                    return True

                if self.json_get_field_by.lower() == "name":
                    wait = WebDriverWait(self.driver, 20)
                    wait.until(EC.visibility_of_element_located((By.NAME, self.json_value_to_find)))
                    wait.until(EC.element_to_be_clickable((By.NAME, self.json_value_to_find)))
                    print(u"Esperar_Elemento: Se visualizo el elemento " + locator)
                    return True

                if self.json_get_field_by.lower() == "xpath":
                    wait = WebDriverWait(self.driver, 20)
                    if MyTextElement is not None:
                        self.json_value_to_find = self.json_value_to_find.format(MyTextElement)
                        print(self.json_value_to_find)
                    else:
                        wait.until(EC.visibility_of_element_located((By.XPATH, self.json_value_to_find)))
                        #wait.until(EC.element_to_be_clickable((By.XPATH, self.json_value_to_find)))
                        print(u"Esperar_Elemento: Se visualizo el elemento " + locator)
                        return True

                if self.json_get_field_by.lower() == "link":
                    wait = WebDriverWait(self.driver, 20)
                    wait.until(EC.visibility_of_element_located((By.PARTIAL_LINK_TEXT, self.json_value_to_find)))
                    wait.until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, self.json_value_to_find)))
                    print(u"Esperar_Elemento: Se visualizo el elemento " + locator)
                    return True

            except TimeoutException:
                print(u"Esperar_Elemento: No presente " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                oReporter.fn_screenshot_allure(self, "get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " TimeoutException")
                #oSelenium.fn_tear_down(self)
                os.environ['status_execution_failed'] = "false"
            except NoSuchElementException:
                print(u"Esperar_Elemento: No presente " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                oReporter.fn_screenshot_allure(self, "get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " NoSuchElementException")
                #oSelenium.fn_tear_down(self)
                os.environ['status_execution_failed'] = "false"

    def fn_wait_element_not_present(self, locator, MyTextElement=None):
        self.json_value_to_find, self.json_get_field_by, self.json_get_obj_name = oGetJson.fn_get_entity(self, locator)
        #time.sleep(1)
        if self.json_get_field_by is None:
            return print("No se encontro el valor en el Json definido")
        else:
            try:
                if self.json_get_field_by.lower() == "id":
                    wait = WebDriverWait(self.driver, 12)
                    wait.until(EC.invisibility_of_element_located((By.ID, self.json_value_to_find)))
                    print(u"Esperar_a que el elemento desaparezca: " + locator)
                    return True

                if self.json_get_field_by.lower() == "name":
                    wait = WebDriverWait(self.driver, 12)
                    wait.until(EC.invisibility_of_element_located((By.ID, self.json_value_to_find)))
                    print(u"Esperar_a que el elemento desaparezca: " + locator)
                    return True

                if self.json_get_field_by.lower() == "xpath":
                    wait = WebDriverWait(self.driver, 12)
                    if MyTextElement is not None:
                        self.json_value_to_find = self.json_value_to_find.format(MyTextElement)
                        print(self.json_value_to_find)

                    wait.until(EC.invisibility_of_element_located((By.ID, self.json_value_to_find)))
                    print(u"Esperar_a que el elemento desaparezca: " + locator)
                    return True

                if self.json_get_field_by.lower() == "link":
                    wait = WebDriverWait(self.driver, 12)
                    wait.until(EC.invisibility_of_element_located((By.ID, self.json_value_to_find)))
                    print(u"Esperar_a que el elemento desaparezca: " + locator)
                    return True

            except TimeoutException:
                print(u"Esperar_Elemento: No presente " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                oReporter.fn_screenshot_allure(self, "get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " TimeoutException")
                #oSelenium.fn_tear_down(self)
                os.environ['status_execution_failed'] = "false"
            except NoSuchElementException:
                print(u"Esperar_Elemento: No presente " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                oReporter.fn_screenshot_allure(self, "get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " NoSuchElementException")
                #oSelenium.fn_tear_down(self)
                os.environ['status_execution_failed'] = "false"

    def fn_specific_time(self, timeLoad=7):
        print("Esperar: Inicia (" + str(timeLoad) + ")")
        try:
            totalWait = 0
            while (totalWait < timeLoad):
                print("Cargando ... intento: " + str(totalWait))
                time.sleep(1)
                totalWait = totalWait + 1
        finally:
            print("Esperar: Carga Finalizada ... ")

    def fn_wait_object(self, locator):
        self.json_get_field_by, self.json_value_to_find, self.json_get_obj_name = oGetJson.fn_get_entity(self, locator)
        time.sleep(1)
        if self.json_get_field_by is None:
            return print("No se encontro el valor en el Json definido")
        else:
            try:
                if self.json_get_field_by == "id":
                    intExist = len(self.driver.find_elements_by_id(locator))

                    if intExist > 0:
                        print("El localizador: " +self.json_value_to_find+ " con nombre: " +self.json_get_obj_name+ " existe")
                        pass
                    else:
                        while intExist < 0:
                            for intIterator in range(60):
                                time.sleep(1)
                                print("Esperando por el localizador: " + self.json_value_to_find + " con nombre: " + self.json_get_obj_name)
                    return True

                if self.json_get_field_by == "name":
                    intExist = len(self.driver.find_elements_by_name(locator))

                    if intExist > 0:
                        print("El localizador: " +self.json_value_to_find+ " con nombre: " +self.json_get_obj_name+ " existe")
                        pass
                    else:
                        while intExist < 0:
                            for intIterator in range(60):
                                time.sleep(1)
                                print(
                                    "Esperando por el localizador: " + self.json_value_to_find + " con nombre: " + self.json_get_obj_name)
                    return True

                if self.json_get_field_by == "xpath":
                    intExist = len(self.driver.find_elements_by_xpath(locator))

                    if intExist > 0:
                        print("El localizador: " +self.json_value_to_find+ " con nombre: " +self.json_get_obj_name+ " existe")
                        pass
                    else:
                        while intExist < 0:
                            for intIterator in range(60):
                                time.sleep(1)
                                print(
                                    "Esperando por el localizador: " + self.json_value_to_find + " con nombre: " + self.json_get_obj_name)
                    return True

                if self.json_get_field_by == "link":
                    intExist = len(self.driver.find_elements_by_link_text(locator))

                    if intExist > 0:
                        print("El localizador: " +self.json_value_to_find+ " con nombre: " +self.json_get_obj_name+ " existe")
                        pass
                    else:
                        while intExist < 0:
                            for intIterator in range(60):
                                time.sleep(1)
                                print(
                                    "Esperando por el localizador: " + self.json_value_to_find + " con nombre: " + self.json_get_obj_name)
                    return True

            except TimeoutException:
                print \
                    (u"Esperar_Elemento: No presente " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
            except NoSuchElementException:
                print \
                    (u"Esperar_Elemento: No presente " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)


    def fn_wait_verify_element_not_exist(self, entity, intentos=None):
        element = oComponentsHelper.fn_get_elements(self, entity)
        resultado = False
        count = 0
        try:
            print(f'Esperando a que termina de cargar la pagina {count}s')
            for intIterator in range(intentos):
                #print("element " + element)
                #print("dsplay " + element.is_displayed())
                #print("len " + len(element))
                if element.is_displayed() and element.is_enabled():
                    time.sleep(1)
                    count = count + 1
                    print(f'Cargando: {count}s')
                else:
                    resultado = True
                    break

            #if resultado == False:
                #raise Exception('ERROR SIGUE EXISTIENDO EL ELEMENTO')

            return resultado

        except StaleElementReferenceException:
            #Continuar con la ejecucion cuando el elemento ya no existe
            print(f'Se termina de cargar la pagina en: {count}s')

        except Exception:
            print("get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
