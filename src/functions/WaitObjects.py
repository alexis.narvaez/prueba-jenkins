import time

from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from functions.Selenium import Selenium as oSelenium
from functions.Reporter import Reporter as oReporter


class Objects(oSelenium):

    def fn_wait_object(self, by, object, MyTextElement=None):
        if by is None:
            return print("No se encontro el valor en el Json definido")
        else:
            try:
                if by == "id":
                    #self.wait_for_object_exist(self, by)
                    wait = WebDriverWait(self.driver, 12)
                    wait.until(EC.visibility_of_element_located((By.ID, object)))
                    wait.until(EC.element_to_be_clickable((By.ID, object)))

                    #browser = webdriver.Firefox(options=options, service=service)
                    #WebDriverWait(browser, 80).until(
                        #EC.visibility_of_element_located((By.CSS_SELECTOR, ".category-list-scroller.touch-scrollable")))

                    print(u"Esperar_Elemento: Se visualizo el elemento " + object)
                    return True

                if by == "name":
                    wait = WebDriverWait(self.driver, 12)
                    wait.until(EC.visibility_of_element_located((By.NAME, object)))
                    wait.until(EC.element_to_be_clickable((By.NAME, object)))
                    print(u"Esperar_Elemento: Se visualizo el elemento " + object)
                    return True

                if by == "xpath":
                    wait = WebDriverWait(self.driver, 12)
                    if MyTextElement is not None:
                        self.json_value_to_find = self.json_value_to_find.format(MyTextElement)
                        print(self.json_value_to_find)

                    #self.wait_for_object_exist(self, by)

                    wait.until(EC.visibility_of_element_located((By.XPATH, object)))
                    wait.until(EC.element_to_be_clickable((By.XPATH, object)))
                    print(u"Esperar_Elemento: Se visualizo el elemento " + object)
                    return True

                if by == "link":
                    wait = WebDriverWait(self.driver, 12)
                    wait.until(EC.visibility_of_element_located((By.PARTIAL_LINK_TEXT, object)))
                    wait.until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, object)))
                    print(u"Esperar_Elemento: Se visualizo el elemento " + object)
                    return True

            except TimeoutException:
                print(u"Esperar_Elemento: No presente " + object + " mediante busqueda por propiedad: " + object + " valor de la propiedad: " + object)
                #oReporter.fn_screenshot_allure(self, "Esperar_Elemento: No presente " + object + " mediante busqueda por propiedad: " + object + " valor de la propiedad: " + object + " TimeoutException")
                #oSelenium.fn_tear_down(self)
                os.environ['status_execution_failed'] = "false"
            except NoSuchElementException:
                print(u"Esperar_Elemento: No presente " + object + " mediante busqueda por propiedad: " + object + " valor de la propiedad: " + object)
                #oReporter.fn_screenshot_allure(self, "Esperar_Elemento: No presente " + object + " mediante busqueda por propiedad: " + object + " valor de la propiedad: " + object + " NoSuchElementException")
                #oSelenium.fn_tear_down(self)
                os.environ['status_execution_failed'] = "false"


    def wait_for_object_exist(self, by, object, obj_name):
        if by is None:
            return print("No se encontro el valor by por el cual buscar")
        else:
            try:
                if by == "id":
                    intExist = len(self.driver.find_elements_by_id(object))

                    if intExist > 0:
                        print("El localizador: " + object + " con nombre: " +obj_name+ " existe")
                        pass
                    else:
                        while intExist < 0:
                            for intIterator in range(60):
                                time.sleep(1)
                                print("Esperando por el localizador: " + object + " con nombre: " +obj_name)
                    return True

                if by == "xpath":
                    intExist = len(self.driver.find_elements_by_xpath(object))

                    if intExist > 0:
                        print("El localizador: " + object + " con nombre: " +obj_name+ " existe")
                        pass
                    else:
                        while intExist < 0:
                            for intIterator in range(60):
                                time.sleep(1)
                                print("Esperando por el localizador: " + object + " con nombre: " +obj_name)
                    return True

                if by == "class":
                    intExist = len(self.driver.find_elements_by_class_name(object))

                    if intExist > 0:
                        print("El localizador: " + object + " con nombre: " +obj_name+ " existe")
                        pass
                    else:
                        while intExist < 0:
                            for intIterator in range(60):
                                time.sleep(1)
                                print("Esperando por el localizador: " + object + " con nombre: " +obj_name)
                    return True

            except TimeoutException:
                print \
                    (u"Esperar_Elemento: No presente " + object + " mediante busqueda por propiedad: " + object + " valor de la propiedad: " + object)

    def fn_page_has_loaded(self):
        driver = self.driver
        print("Checking if {} page is loaded.".format(self.driver.current_url))
        page_state = driver.execute_script('return document.readyState;')
        yield
        WebDriverWait(driver, 12).until(lambda driver: page_state == 'complete')
        assert page_state == 'complete', "No se completo la carga"
        return self.driver

    def fn_wait_verify_element_exist(self, by, object, obj_name, intentos=300):
        element = object
        resultado = False
        count = 0
        if by == "id":
            element = self.driver.find_elements_by_id(object)
        elif by == "xpath":
            element = self.driver.find_elements_by_xpath(object)
        elif by == "class":
            element = self.driver.find_elements_by_class_name(object)
        elif by == "css":
            element = self.driver.find_elements_by_css_selector(object)

        try:
            for intIterator in range(intentos):
                if element.__sizeof__() > 0:
                    #contador = element.__sizeof__()
                    resultado = True
                    break
                else:
                    time.sleep(1)
                    count = count + 1
                    print(f'Esperando a que el elemento con nombre: {obj_name} exista segundos: {count}s')

            if resultado == False:
                raise Exception(f'El elemento sigue existiendo, nombre del objeto {obj_name} buscado por {by} localizador {object}')
            return resultado

        except Exception:
            print(
                "get_text: No se encontro el objeto: " + obj_name + " mediante busqueda por propiedad: " + by + " valor de la propiedad: " + object)


    def fn_wait_verify_element_not_exist(self, by, object, obj_name, intentos=300):
        element = object
        resultado = False
        count = 0
        if by == "id":
            element = self.driver.find_elements_by_id(object)
        elif by == "xpath":
            element = self.driver.find_elements_by_xpath(object)
        elif by == "class":
            element = self.driver.find_elements_by_class_name(object)
        elif by == "css":
            element = self.driver.find_elements_by_css_selector(object)

        try:
            for intIterator in range(intentos):
                if element.is_enabled():
                    #contador = element.__sizeof__()
                    time.sleep(1)
                    count = count + 1
                    print(f'Esperando a que el elemento con nombre: {obj_name} NO exista segundos: {count}s')
                else:
                    resultado = True
                    break

            if resultado == False:
                raise Exception(f'El elemento sigue existiendo, nombre del objeto {obj_name} buscado por {by} localizador {object}')
            return resultado

        except Exception:
            print(
                "get_text: No se encontro el objeto: " + obj_name + " mediante busqueda por propiedad: " + by + " valor de la propiedad: " + object)
