from selenium.webdriver import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium import webdriver
from functions.Selenium import Selenium
from functions.Initialize import Initialize

import os
from functions.Selenium import Selenium as oSelenium
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException, ElementNotInteractableException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import Select
from functions.WaitObjects import Objects as oObject
from functions.Validations import Validations as oValidation
from functions.GetJsons import GetJsons as oGetJson
from functions.Reporter import Reporter as oReporter


import json
import pytest
import time


class ComponentsHelper(oSelenium):

    def __init__(self, json_get_field_by, json_value_to_find, json_get_obj_name):
        self.json_get_field_by = json_get_field_by
        self.json_value_to_find = json_value_to_find
        self.json_get_obj_name = json_get_obj_name

    def fn_get_elements(self, entity, add_locator=None, MyTextElement=None):
        get_entity = oGetJson.fn_get_entity(self, entity)

        if get_entity is None:
            print("No se encontro el valor en el Json definido")
        else:
            try:
                if self.json_get_field_by.lower() == "id":
                    elements = self.driver.find_element_by_id(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "name":
                    # print(self.json_value_to_find)
                    elements = self.driver.find_element_by_name(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "xpath":
                    if MyTextElement is not None:
                        self.json_value_to_find = self.json_value_to_find.format(MyTextElement)
                        #print(self.json_value_to_find)
                        elements = self.driver.find_element_by_xpath(self.json_value_to_find)
                    elif add_locator is not None:
                        element = (self.json_value_to_find)
                        print(element + add_locator)
                        elements = self.driver.find_element_by_xpath(element + add_locator)
                    else:
                        elements = self.driver.find_element_by_xpath(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "link":
                    elements = self.driver.find_element_by_partial_link_text(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "css":
                    elements = self.driver.find_element_by_css_selector(self.json_value_to_find)

                print("get_elements: " + self.json_value_to_find)
                # print(elements)
                return elements

            except NoSuchElementException:
                #oReporter.fn_screenshot_allure(self, "get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " NoSuchElementException")
                print("get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " NoSuchElementException")
                #os.environ['status_execution_failed'] = "false"
                #oSelenium.fn_tear_down(self)
            except TimeoutException:
                #oReporter.fn_screenshot_allure(self, "get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " TimeoutException")
                print("get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " TimeoutException")
                #os.environ['status_execution_failed'] = "false"
                #oSelenium.fn_tear_down(self)



    def fn_send_key_text(self, entity, text, MyTextElement=None):

        get_entity = oGetJson.fn_get_entity(self, entity)

        if get_entity is None:
            print("No se encontro el valor en el Json definido")
        else:
            try:
                if self.json_get_field_by.lower() == "id":
                    elements = self.driver.find_element_by_id(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "name":
                    # print(self.json_value_to_find)
                    elements = self.driver.find_element_by_name(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "xpath":
                    if MyTextElement is not None:
                        self.json_value_to_find = self.json_value_to_find.format(MyTextElement)
                        #print(self.json_value_to_find)
                    elements = self.driver.find_element_by_xpath(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "link":
                    elements = self.driver.find_element_by_partial_link_text(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "css":
                    elements = self.driver.find_element_by_css_selector(self.json_value_to_find)

                print("get_elements: " + self.json_value_to_find)
                # print(elements)
                #oObject.fn_wait_object(self, self.json_get_field_by.lower(), self.json_value_to_find)
                #oObject.wait_for_object_exist(self, self.json_get_field_by.lower(), self.json_value_to_find, self.json_get_obj_name)
                #oObject.fn_page_has_loaded(self)
                #oObject.fn_wait_verify_element_exist(self, self.json_get_field_by.lower(), self.json_value_to_find, self.json_get_obj_name)
                #elements.clear()
                time.sleep(0.5)
                elements.send_keys(text)
                #time.sleep(0.5)
                return elements

            except NoSuchElementException:
                print("Metodo SendKeys: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " NoSuchElementException")
                oReporter.fn_screenshot_allure(self, "get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " NoSuchElementException")
                #oSelenium.fn_tear_down(self)
                os.environ['status_execution_failed'] = "false"
            except TimeoutException:
                print("Metodo SendKeys: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " TimeoutException")
                oReporter.fn_screenshot_allure(self, "get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " TimeoutException")
                #oSelenium.fn_tear_down(self)
                os.environ['status_execution_failed'] = "false"
            except StaleElementReferenceException:
                print("Metodo SendKeys: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " StaleElementReferenceException")
                oReporter.fn_screenshot_allure(self, "get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " TimeoutException")
                #oSelenium.fn_tear_down(self)
                os.environ['status_execution_failed'] = "false"

    def fn_click_element(self, entity, add_locator=None, MyTextElement=None):
        get_entity = oGetJson.fn_get_entity(self, entity)

        if get_entity is None:
            print("No se encontro el valor en el Json definido")
        else:
            try:
                if self.json_get_field_by.lower() == "id":
                    elements = self.driver.find_element_by_id(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "name":
                    # print(self.json_value_to_find)
                    elements = self.driver.find_element_by_name(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "xpath":
                    if MyTextElement is not None:
                        self.json_value_to_find = self.json_value_to_find.format(MyTextElement)
                        #print(self.json_value_to_find)
                        elements = self.driver.find_element_by_xpath(self.json_value_to_find)
                    elif add_locator is not None:
                        element = (self.json_value_to_find)
                        #print(element + add_locator)
                        elements = self.driver.find_element_by_xpath(element + add_locator)
                    else:
                        elements = self.driver.find_element_by_xpath(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "link":
                    elements = self.driver.find_element_by_partial_link_text(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "css":
                    elements = self.driver.find_element_by_css_selector(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "clase_name":
                    elements = self.driver.find_element_by_class_name(self.json_value_to_find)

                if add_locator is not None:
                    print("get_elements: " + self.json_value_to_find + str(add_locator))
                else:
                    print("get_elements: " + self.json_value_to_find)
                # print(elements)
                time.sleep(0.5)
                #oObject.fn_wait_object(self, self.json_get_field_by.lower(), self.json_value_to_find)
                #oObject.wait_for_object_exist(self, self.json_get_field_by.lower(), self.json_value_to_find, self.json_get_obj_name)
                oObject.fn_page_has_loaded(self)
                #oObject.fn_wait_verify_element_exist(self, self.json_get_field_by.lower(), self.json_value_to_find, self.json_get_obj_name)
                elements.click()
                #oObject.fn_wait_verify_element_not_exist(self, self.json_get_field_by.lower(), self.json_value_to_find, self.json_get_obj_name)
                #time.sleep(0.5)
                return elements

            except NoSuchElementException:
                oReporter.fn_screenshot_allure(self, "get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " NoSuchElementException")
                print("Metodo Click: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " NoSuchElementException")
                os.environ['status_execution_failed'] = "false"
                #oSelenium.fn_tear_down(self)
            except TimeoutException:
                oReporter.fn_screenshot_allure(self, "get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " TimeoutException")
                print("Metodo Click: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " TimeoutException")
                os.environ['status_execution_failed'] = "false"
                #oSelenium.fn_tear_down(self)
            except StaleElementReferenceException:
                oReporter.fn_screenshot_allure(self, "get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " StaleElementReferenceException")
                print("Metodo Click: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " StaleElementReferenceException")
                os.environ['status_execution_failed'] = "false"
                #oSelenium.fn_tear_down(self)
            except ElementNotInteractableException:
                oReporter.fn_screenshot_allure(self, "get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " ElementNotInteractableException")
                print("Metodo Click: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " StaleElementReferenceException")
                os.environ['status_execution_failed'] = "false"
                #oSelenium.fn_tear_down(self)

    def fn_get_select_elements(self, entity):
        get_entity = oGetJson.fn_get_entity(self, entity)

        if get_entity is None:
            print("No se encontro el valor en el Json definido")
        else:
            try:
                if self.json_get_field_by.lower() == "id":
                    select = Select(self.driver.find_element_by_id(self.json_value_to_find))

                elif self.json_get_field_by.lower() == "name":
                    select = Select(self.driver.find_element_by_name(self.json_value_to_find))

                elif self.json_get_field_by.lower() == "xpath":
                    select = Select(self.driver.find_element_by_xpath(self.json_value_to_find))

                elif self.json_get_field_by.lower() == "link":
                    select = Select(self.driver.find_element_by_partial_link_text(self.json_value_to_find))

                print("get_select_elements: " + self.json_value_to_find)
                time.sleep(0.5)
                #oObject.fn_wait_object(self, self.json_get_field_by.lower(), self.json_value_to_find)
                #oObject.wait_for_object_exist(self, self.json_get_field_by.lower(), self.json_value_to_find, self.json_get_obj_name)
                oObject.fn_page_has_loaded(self)
                #oObject.fn_wait_verify_element_exist(self, self.json_get_field_by.lower(), self.json_value_to_find, self.json_get_obj_name)
                return select

            # USO

            #       select by visible text  #       select.select_by_visible_text('Banana')

            #       select by value  #       select.select_by_value('1')

            except NoSuchElementException:
                oReporter.fn_screenshot_allure(self, "get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " NoSuchElementException")
                print("Metodo Select: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " NoSuchElementException")
                os.environ['status_execution_failed'] = "false"
                #oSelenium.fn_tear_down(self)
            except TimeoutException:
                oReporter.fn_screenshot_allure(self, "get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " TimeoutException")
                print("Metodo Select: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " TimeoutException")
                os.environ['status_execution_failed'] = "false"
                #oSelenium.fn_tear_down(self)

    def fn_select_by_text(self, entity, text):
        get_entity = oGetJson.fn_get_entity(self, entity)

        if get_entity is None:
            print("No se encontro el valor en el Json definido")
        else:
            try:
                if self.json_get_field_by.lower() == "id":
                    select = Select(self.driver.find_element_by_id(self.json_value_to_find))

                elif self.json_get_field_by.lower() == "name":
                    select = Select(self.driver.find_element_by_name(self.json_value_to_find))

                elif self.json_get_field_by.lower() == "xpath":
                    select = Select(self.driver.find_element_by_xpath(self.json_value_to_find))

                elif self.json_get_field_by.lower() == "link":
                    select = Select(self.driver.find_element_by_partial_link_text(self.json_value_to_find))

                print("get_select_elements: " + self.json_value_to_find)
                time.sleep(0.5)
                select.select_by_visible_text(text)
                return select

            # USO

            #       select by visible text  #       select.select_by_visible_text('Banana')

            #       select by value  #       select.select_by_value('1')

            except NoSuchElementException:

                oReporter.fn_screenshot_allure(self, "Metodo Select: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " NoSuchElementException")
                print("Metodo Select: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " NoSuchElementException")
                os.environ['status_execution_failed'] = "false"
                #oSelenium.fn_tear_down(self)

            except TimeoutException:
                oReporter.fn_screenshot_allure(self, "Metodo Select: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " TimeoutException")
                print("Metodo Select: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " TimeoutException")
                os.environ['status_execution_failed'] = "false"
                #oSelenium.fn_tear_down(self)

    def fn_get_elements_selenium(self, entity, MyTextElement=None):
        get_entity = oGetJson.fn_get_entity(self, entity)

        if get_entity is None:
            print("No se encontro el valor en el Json definido")
        else:
            try:
                if self.json_get_field_by.lower() == "id":
                    elements = self.driver.find_elements_by_id(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "name":
                    # print(self.json_value_to_find)
                    elements = self.driver.find_elements_by_name(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "xpath":
                    if MyTextElement is not None:
                        self.json_value_to_find = self.json_value_to_find.format(MyTextElement)
                    elements = self.driver.find_elements_by_xpath(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "link":
                    elements = self.driver.find_elements_by_partial_link_text(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "css":
                    elements = self.driver.find_elements_by_css_selector(self.json_value_to_find)

                print("get_elements: " + self.json_value_to_find)
                # print(elements)
                return elements

            except NoSuchElementException:
                oReporter.fn_screenshot_allure(self, "get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " NoSuchElementException")
                print("get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " NoSuchElementException")
                os.environ['status_execution_failed'] = "false"
                #oSelenium.fn_tear_down(self)

            except TimeoutException:
                oReporter.fn_screenshot_allure(self, "get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " TimeoutException")
                print("get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " TimeoutException")
                os.environ['status_execution_failed'] = "false"
                #oSelenium.fn_tear_down(self)

    def fn_select_element_dropdown(self, locator):
        self.json_value_to_find, self.json_get_field_by, self.json_get_obj_name = oGetJson.fn_get_entity(self, locator)
        if self.json_get_field_by is None:
            return print("No se encontro el valor en el Json definido")
        else:
            try:
                if self.json_get_field_by.lower() == "id":
                    wait = WebDriverWait(self.driver, 20)
                    wait.until(EC.visibility_of_element_located((By.ID, self.json_value_to_find)))
                    wait.until(EC.element_to_be_clickable((By.ID, self.json_value_to_find)))
                    element = self.driver.find_element_by_id(self.json_value_to_find)
                    actions = ActionChains(self.driver)
                    actions.move_to_element(element).perform()
                    wait.until(EC.visibility_of_element_located((By.XPATH, self.json_value_to_find))).click()
                    print(u"Esperar_Elemento: Se visualizo el elemento " + locator)
                    return True

                if self.json_get_field_by.lower() == "xpath":
                    wait = WebDriverWait(self.driver, 20)
                    wait.until(EC.visibility_of_element_located((By.XPATH, self.json_value_to_find)))
                    element = self.driver.find_element_by_xpath(self.json_value_to_find)
                    actions = ActionChains(self.driver)
                    actions.move_to_element(element).perform()
                    wait.until(EC.visibility_of_element_located((By.XPATH, self.json_value_to_find))).click()
                    print(u"Esperar_Elemento: Se visualizo el elemento " + locator)
                    return True

            except TimeoutException:
                print(
                    u"Esperar_Elemento: No presente " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                oReporter.fn_screenshot_allure(self,
                                               "get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " TimeoutException")
                # oSelenium.fn_tear_down(self)
                os.environ['status_execution_failed'] = "false"
            except NoSuchElementException:
                print(
                    u"Esperar_Elemento: No presente " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                oReporter.fn_screenshot_allure(self,
                                               "get_text: No se encontro el objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find + " NoSuchElementException")
                # oSelenium.fn_tear_down(self)
                os.environ['status_execution_failed'] = "false"



    # Only the years from 1997 to 2004
    def fn_select_dates(self, year):
        wait = WebDriverWait(self.driver, 20)
        wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@class='current']"))).click()
        element2= wait.until(EC.visibility_of_element_located((By.XPATH, "//table[contains(@class,'years')]")))
        rows = element2.find_elements(By.TAG_NAME, "td")
        for row in rows:
            if(year == row.text):
                wait = WebDriverWait(self.driver, 20)
                wait.until(EC.element_to_be_clickable((By.XPATH, f"//td[contains(.,{row.text})]"))).click()
                time.sleep(1)
                return True


    def fn_scroll_inside(self, locator):
        self.json_value_to_find, self.json_get_field_by, self.json_get_obj_name = oGetJson.fn_get_entity(self, locator)
        if self.json_get_field_by is None:
            return print("No se encontro el valor en el Json definido")
        else:
            if self.json_get_field_by.lower() == "id":
                wait = WebDriverWait(self.driver, 20)
                wait.until(EC.visibility_of_element_located((By.ID, self.json_value_to_find)))
                wait.until(EC.element_to_be_clickable((By.ID, self.json_value_to_find)))
                recentList = self.driver.find_elements_by_xpath(self.json_value_to_find)
                for list in recentList:
                    self.driver.execute_script("arguments[0].scrollTop = arguments[0].scrollHeight", list)
                print(u"Esperar_Elemento: Se visualizo el elemento " + locator)
                return True

            if self.json_get_field_by.lower() == "xpath":
                wait = WebDriverWait(self.driver, 20)
                wait.until(EC.visibility_of_element_located((By.XPATH, self.json_value_to_find)))
                recentList = self.driver.find_elements_by_xpath(self.json_value_to_find)
                for list in recentList:
                    self.driver.execute_script("arguments[0].scrollTop = arguments[0].scrollHeight", list)
                print(u"Esperar_Elemento: Se visualizo el elemento " + locator)
                return True