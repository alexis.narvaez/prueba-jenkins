import os
import time

from webdriver_manager.utils import ChromeType

from functions.Initialize import Initialize
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.common.exceptions import InvalidSessionIdException



class Selenium(Initialize):

    def fn_open_browser(self, URL=Initialize.URL, browser=Initialize.BROWSER):
        os.environ['status_execution_failed'] = "True"
        print(Initialize.basedir)
        #self.windows = {}
        print("*************************")
        print(browser)
        print("*************************")
        print(Initialize.basedir + "/drivers/chromedriver")

        STR_CHROME_BROWSER = "CHROME"
        STR_FIREFOX_BROWSER = "FIREFOX"
        STR_CHROME_HEADLESS = "HEADLESS"
        browser_upper = browser.upper()

        if browser_upper == STR_CHROME_BROWSER:
            options_chrome = ChromeOptions()
            options_chrome.add_argument('start-maximized')
            options_chrome.add_argument('--disable-gpu')
            options_chrome.add_argument('--no-sandbox')
            options_chrome.add_argument('--disable-dev-shm-usage')
            self.driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
            #self.driver = webdriver.Chrome(ChromeDriverManager().download_and_install())
            #self.driver = webdriver.Chrome(ChromeDriverManager(chrome_type=ChromeType.GOOGLE).install())
            self.driver.delete_all_cookies()
            self.driver.maximize_window()
            self.driver.implicitly_wait(5)
            self.driver.get(URL)
            self.vmain = self.driver.window_handles[0]
            self.window = {'Main Window': self.driver.window_handles[0]}
            print(Selenium.fn_main_handle(self))
            self.nwindows = 0
            return self.driver

        elif browser_upper == STR_FIREFOX_BROWSER:
            self.driver = webdriver.Firefox()
            options_firefox = FirefoxOptions()
            #self.driver = webdriver.Firefox(options=options_firefox, executable_path=Initialize.basedir + "/drivers/geckodriver")
            self.driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
            self.driver.implicitly_wait(12)
            self.driver.maximize_window()
            self.driver.get(URL)
            self.window = {'Main Window': self.driver.window_handles[0]}
            print(self.window)
            return self.driver

        elif browser_upper == STR_CHROME_HEADLESS:
            options_headless = ChromeOptions()
            options_headless.add_argument('headless')
            #options_headless.add_argument('--start-maximized')
            options_headless.add_argument("window-size=1920,1080")
            options_headless.add_argument('--lang=es')
            options_headless.add_argument('--no-sandbox')
            options_headless.add_argument('--disable-dev-shm-usage')
            options_headless.add_argument('--disable-gpu')
            #self.driver = webdriver.Chrome(options=options_headless, executable_path=Initialize.basedir + "/drivers/chromedriver")
            #self.driver = webdriver.Chrome(options=options_headless, executable_path="C:/Users/Fabián Solano/.wdm/drivers/chromedriver/win32/100.0.4896.60/chromedriver.exe")
            self.driver = webdriver.Chrome(options=options_headless, executable_path=ChromeDriverManager().install())
            self.driver.delete_all_cookies()
            self.driver.implicitly_wait(7)
            self.driver.get(URL)
            self.vmain = self.driver.window_handles[0]
            self.window = {'Main Window': self.driver.window_handles[0]}
            self.nwindows = 0
            return self.driver

    def fn_tear_down(self):
        #oReporter.fn_screenshot_allure(self, "Ultimo paso antes de cerrar el driver")
        print("Se cierra el Driver")
        print("Se cierra la coneccion con el Driver")
        time.sleep(1)
        self.driver.close()
        self.driver.quit()

    def fn_tear_down_driver(self):
       print("Se cierra la coneccion con el Driver")
       #oReporter.fn_screenshot_allure(self, "Algo salio mal")
       time.sleep(1)
       #self.driver.close()
       self.driver.quit()

    def fn_main_handle(self):
        self.window = {}
        self.window = {'Main Window': self.driver.window_handles[0]}
        return self.window

    def fn_get_title(self):
        return self.driver.title()

    def fn_refresh_driver(self):
        return self.driver.refresh()

    def fn_get_session_id(self):
        return self.driver.session_id()

    def fn_get_driver(self):
        return self.driver

    def fn_get_url(self):
        return self.driver.current_url

    def fn_set_url(self, url):
        return self.driver.get(url)