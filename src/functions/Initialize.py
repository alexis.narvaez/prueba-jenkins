import os

class Initialize():
    basedir = os.path.abspath(os.path.join(__file__, "../.."))
    DateFormat = "%d/%m/%Y"
    formatHour = "%H%M%S"

    #JasonData
    Json = basedir + u'/pages'
    #JsonResponseData = basedir + u'\datajson'

    Environment = 'updates'

    #Browser for executing tests
    BROWSER = u'chrome'

    #Evidence Folder
    Path_Evidence = basedir + u'/data/evidence'

    #Excel Data
    Excel_Data = basedir + u'/data/Data.xlsx'

    if Environment == 'dev':
        URL = f'http://localhost:8072/web/login'
        User = 'fabian'
        Password = 'pwd1234'
    elif Environment == 'updates':
        URL = f'https://batman.com/'
        User = 'fabian'
        Password = 'pwd1234'
    elif Environment == 'local':
        URL = f'http://localhost:8069/'
        User = 'fabian'
        Password = 'pwd1234'