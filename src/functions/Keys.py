from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By

from functions.Selenium import Selenium as oSelenium
from functions.GetJsons import GetJsons as oGetJson
from functions.ComponentsHelper import ComponentsHelper as oComponentsHelper
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains as oActionChain
import time
#import pyautogui


class Keysr(oSelenium):

    def fn_select_by_text(self, entity, text):
        oComponentsHelper.fn_get_select_elements(self, entity).select_by_visible_text(text)

    def fn_send_key_text(self, entity, text):
        oComponentsHelper.fn_get_select_elements(self, entity).clear()
        oComponentsHelper.fn_get_select_elements(self, entity).send_keys(text)

    def fn_send_especific_keys(self, element, key):
        key_r = str(key.lower())
        if key_r == 'enter':
            oComponentsHelper.fn_get_elements(self, element).send_keys(Keys.ENTER)
        if key_r == 'tab':
            oComponentsHelper.fn_get_elements(self, element).send_keys(Keys.TAB)
        if key_r == 'space':
            oComponentsHelper.fn_get_elements(self, element).send_keys(Keys.SPACE)
        time.sleep(3)

    def fn_send_key_text_action(self, locator, text):
        self.json_get_field_by, self.json_value_to_find, self.json_get_obj_name = oGetJson.fn_get_entity(self, locator)
        if self.json_get_field_by is None:
            return print("No se encontro el valor en el Json definido")
        else:
            try:
                if self.json_value_to_find.lower() == "name":
                    Localizador = self.driver.find_element(By.NAME, self.json_value_to_find)
                    action = oActionChain.click(self.driver)
                    # action.click(Localizador)
                    # action.perform(Localizador)
                    action.send_keys(Localizador, text)
                    action.perform(Localizador)
                    print(u"send key: " + self.json_get_obj_name)
                    return True

            except TimeoutException:
                print(u"send key: No presente " + self.json_get_obj_name)
                #oSelenium.tearDown(self)
                return None
'''
    def fn_send_key_text_robot(self, key, text=None):
        key_r = key.lower()
        if key_r == "click":
            pyautogui.click()

        if key_r == "tab":
            pyautogui.press("tab")

        if key_r == "write":
            pyautogui.write(text)
'''