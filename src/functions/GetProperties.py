

from functions.Selenium import Selenium as oSelenium
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import Select
from functions.GetJsons import GetJsons as oGetJson
from functions.Reporter import Reporter as oReporter
import os

class GetProperty(oSelenium):

    def fn_get_options(self, locator):

        self.json_value_to_find, self.json_get_field_by, self.json_get_obj_name = oGetJson.fn_get_entity(self, locator)
        if self.json_get_field_by is None:
            return print("No se encontro el valor en el Json definido")
        else:
            try:
                if self.json_get_field_by.lower() == "id":
                    selector = Select(self.driver.find_element_by_id(self.json_value_to_find))
                    options = len(selector.options)
                    return options

                if self.json_get_field_by.lower() == "xpath":
                    selector = Select(self.driver.find_element_by_xpath(self.json_value_to_find))
                    #options = selector.options
                    options = selector.options
                    for index in range(1, len(options) - 1):
                        print(options[index].text)
                    return options

                if self.json_get_field_by.lower() == "class":
                    selector = Select(self.driver.find_element_by_class_name(self.json_value_to_find))
                    options = selector.options
                    return options

            except TimeoutException:
                oReporter.fn_screenshot_allure(self, "No se encontraron las opciones del combo" + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                print(u"No se encontraron las opciones del combo" + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                os.environ['status_execution_failed'] = "false"
            except NoSuchElementException:
                oReporter.fn_screenshot_allure(self, "No se encontraron las opciones del combo" + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                print(u"No se encontraron las opciones del combo" + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                os.environ['status_execution_failed'] = "false"


    def fn_get_text(self, entity, MyTextElement = None):
        get_entity = oGetJson.fn_get_entity(self, entity)

        if get_entity is None:
            print("No se encontro el valor en el Json definido")
        else:
            try:
                if self.json_get_field_by.lower() == "id":
                    elements = self.driver.find_element_by_id(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "name":
                    elements = self.driver.find_element_by_name(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "xpath":
                    if MyTextElement is not None:
                        self.json_ValueToFind = self.json_value_to_find.format(MyTextElement)
                        print (self.json_value_to_find)
                    elements = self.driver.find_element_by_xpath(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "link":
                    elements = self.driver.find_element_by_partial_link_text(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "css":
                    elements = self.driver.find_element_by_css_selector(self.json_value_to_find)

                print("get_text: " + self.json_value_to_find)
                print("Text Value : " + elements.text)
                return elements.text

            except NoSuchElementException:
                oReporter.fn_screenshot_allure(self, "No se encontro la propiedad text del elemento:" + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                print("No se encontro la propiedad text del elemento:" + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                os.environ['status_execution_failed'] = "false"
            except TimeoutException:
                oReporter.fn_screenshot_allure(self, "No se encontro la propiedad text del elemento:" + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                print("No se encontro la propiedad text del elemento:" + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                os.environ['status_execution_failed'] = "false"

    def fn_get_attribute(self, entity, property, MyTextElement = None):
        global element
        get_entity = oGetJson.fn_get_entity(self, entity)

        if get_entity is None:
            print("No se encontro el valor en el Json definido")
        else:
            try:
                if self.json_get_field_by.lower() == "id":
                    element = self.driver.find_element_by_id(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "name":
                    element = self.driver.find_element_by_name(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "xpath":
                    if MyTextElement is not None:
                        self.json_ValueToFind = self.json_value_to_find.format(MyTextElement)
                        print (self.json_value_to_find)
                    element = self.driver.find_element_by_xpath(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "link":
                    element = self.driver.find_element_by_partial_link_text(self.json_value_to_find)

                elif self.json_get_field_by.lower() == "css":
                    element = self.driver.find_element_by_css_selector(self.json_value_to_find)

                print("get_text: " + self.json_value_to_find)
                print("Text property : " + element.get_attribute(property))
                return element.get_attribute(property)

            except NoSuchElementException:
                oReporter.fn_screenshot_allure(self, "No se encontro la propiedad value del objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                print("No se encontro la propiedad value del objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                os.environ['status_execution_failed'] = "false"
            except TimeoutException:
                oReporter.fn_screenshot_allure(self, "No se encontro la propiedad value del objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                print("No se encontro la propiedad value del objeto: " + self.json_get_obj_name + " mediante busqueda por propiedad: " + self.json_get_field_by + " valor de la propiedad: " + self.json_value_to_find)
                os.environ['status_execution_failed'] = "false"
           # except:
            #    print("Algo raro")