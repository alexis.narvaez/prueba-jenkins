
from functions.Selenium import Selenium as oSelenium
from selenium.webdriver.support.ui import WebDriverWait
from functions.Jscript import Jscript as oJscript
from functions.ComponentsHelper import ComponentsHelper as oComponentsHelper

class WindowHandle(oSelenium):

    def fn_switch_to_iframe(self, locator):
        iframe = oComponentsHelper.fn_get_elements(self, locator)
        self.driver.switch_to.frame(iframe)
        print(f"Se realizó el switch a {locator}")

    def fn_switch_to_parentFrame(self):
        self.driver.switch_to.parent_frame()

    def fn_switch_new_window(self):
        self.driver.switch_to.window(self.driver.window_handles[1])

    def fn_switch_to_default_content(self):
        self.driver.switch_to.default_content()