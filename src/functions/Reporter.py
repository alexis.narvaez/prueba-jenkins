
from functions.Initialize import Initialize
from functions.Selenium import Selenium as oSelenium
import allure
import time
import datetime
import re
import os

global_time = time.strftime(Initialize.formatHour)  # formato 24 houras

class Reporter(oSelenium):

    def fn_get_actual_hour(self):
        self.hour = time.strftime(Initialize.formatHour)  # formato 24 horas
        return self.hour

    def fn_make_path_directory(self):
        day = time.strftime("%d_%m_%Y")  # formato aaaa/mm/dd
        general_path = Initialize.Path_Evidence
        driver_test = Initialize.BROWSER
        test_case = self.__class__.__name__
        actual_hour = Reporter.fn_get_actual_hour(self)
        tc_name = re.search("Context", test_case)
        print(tc_name)
        if (tc_name):
            path = f"{general_path}/{day}/{driver_test}/{actual_hour}/"
            print(path)
        else:
            path = f"{general_path}/{day}/{test_case}/{driver_test}/{actual_hour}/"
            print(path)

        if not os.path.exists(path):  # si no existe el directorio lo crea
            os.makedirs(path)

        return path

    def fn_screenshoot_on_browser(self, TestCase="Captura"):
        PATH = Reporter.fn_make_path_directory(self)
        time.sleep(1)
        img = f'{PATH}/{TestCase}_(' + str(Reporter.fn_get_actual_hour(self)) + ')' + '.png'
        self.driver.get_screenshot_as_file(img)
        print(img)
        return img

    def fn_screenshot_allure(self, str_descripcion):
        time.sleep(1)
        allure.attach(self.driver.get_screenshot_as_png(), str_descripcion, attachment_type=allure.attachment_type.PNG)

    def fn_info_allure():
        allure.step("El caso de prueba fallo")
