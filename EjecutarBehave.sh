#!/bin/bash

cd ./src

rm -r allure-results

behave --junit --tags=-skip --no-skipped

#Ejecutar todos los features
behave -f allure_behave.formatter:AllureFormatter -o allure-results -f pretty ./features

#Correr un archivo .feature
behave -f allure_behave.formatter:AllureFormatter -o allure-results -f pretty ./features/test_test.feature

#Generar y abrir el reporte
allure generate ./allure-results --output ./allure-report --clean && allure open --port 5000

#Abrir el reporte debes estar en src
allure open --port 5000

#Ejecutar casos fallados
behave -f allure_behave.formatter:AllureFormatter --tags=hp --tags-skip --no-skipped -o allure-results -f pretty ./features/WebScenarios.feature

Failing scenarios:
  features/WebScenarios.feature:53  Validar una compra de lentes opticos monofocal + AR verde + Hi index
  features/WebScenarios.feature:189  Validar una compra de lentes opticos monofocal + Fotocromatica cafe + Hi index
  features/WebScenarios.feature:368  Validar una compra de lentes opticos progresivos + AR azul
